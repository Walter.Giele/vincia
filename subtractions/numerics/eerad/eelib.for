***********************************************************************
C SFEE2J CALCULATES THE PROCESS E+ E- --> G,Z --> Q + QB
C INCLUDES: SPIN AVERAGES, FINAL STATE STATISTICS, COUPLINGS
C**********************************************************************
      SUBROUTINE SFEE2J(EETOT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /GENERA/ W,Q,QCDL,EM,GS,SW2,RMZ,RGZ,NJET,NF,NUTF,NDTF,ICON
      COMMON /RESULT/ EEGG,EEGZ,EEZZ
      DIMENSION R(3,2)
C
      CALL EEQ2G0(1,2,3,4,R(1,1),SW2,RMZ,RGZ)
C
      FACTOR=EM**4/4D0
C
      EEGG=(NUTF*R(1,1)+NDTF*R(1,2))*FACTOR
      EEGZ=(NUTF*R(2,1)+NDTF*R(2,2))*FACTOR
      EEZZ=(NUTF*R(3,1)+NDTF*R(3,2))*FACTOR
      EETOT=EEGG+EEGZ+EEZZ
      END
C
C**********************************************************************
C SFEE3J CALCULATES THE PROCESS E+ E- --> G,Z --> Q + QB + G
C INCLUDES: SPIN AVERAGES, FINAL STATE STATISTICS, COUPLINGS
C**********************************************************************
      SUBROUTINE SFEE3J(EETOT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /GENERA/ W,Q,QCDL,EM,GS,SW2,RMZ,RGZ,NJET,NF,NUTF,NDTF,ICON
      COMMON /RESULT/ EEGG,EEGZ,EEZZ
      DIMENSION R(3,2)
C
      CALL EEQ2G1(1,2,3,4,5,R(1,1),SW2,RMZ,RGZ)
C
      FACTOR=EM**4*GS**2/4D0
C
      EEGG=(NUTF*R(1,1)+NDTF*R(1,2))*FACTOR
      EEGZ=(NUTF*R(2,1)+NDTF*R(2,2))*FACTOR
      EEZZ=(NUTF*R(3,1)+NDTF*R(3,2))*FACTOR
      EETOT=EEGG+EEGZ+EEZZ
      END
C
C**********************************************************************
C SFEE4J CALCULATES THE PROCESS E+ E- --> G,Z --> Q + QB + ( GG OR QQB)
C INCLUDES: SPIN AVERAGES, FINAL STATE STATISTICS, COUPLINGS
C**********************************************************************
      SUBROUTINE SFEE4J(EETOT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /GENERA/ W,Q,QCDL,EM,GS,SW2,RMZ,RGZ,NJET,NF,NUTF,NDTF,ICON
      COMMON /RESULT/ EEGG,EEGZ,EEZZ
      DIMENSION RGG(3,2),RQQ(3,5)
C
      CALL EEQ2G2(1,2,3,4,5,6,RGG(1,1),SW2,RMZ,RGZ)
      CALL EEQ4(1,2,3,4,5,6,RQQ(1,1),SW2,RMZ,RGZ)
C
      FACTOR=EM**4*GS**4/4D0
C
      EEGG=(+NUTF*RGG(1,1)/2D0+NDTF*RGG(1,2)/2D0+NUTF*RQQ(1,1)/4D0
     .      +NDTF*RQQ(1,2)/4D0+0.5D0*NUTF*(NUTF-1)*RQQ(1,3)
     .      +0.5D0*NDTF*(NDTF-1)*RQQ(1,4)+NUTF*NDTF*RQQ(1,5))*FACTOR
      EEGZ=(+NUTF*RGG(2,1)/2D0+NDTF*RGG(2,2)/2D0+NUTF*RQQ(2,1)/4D0
     .      +NDTF*RQQ(2,2)/4D0+0.5D0*NUTF*(NUTF-1)*RQQ(2,3)
     .      +0.5D0*NDTF*(NDTF-1)*RQQ(2,4)+NUTF*NDTF*RQQ(2,5))*FACTOR
      EEZZ=(+NUTF*RGG(3,1)/2D0+NDTF*RGG(3,2)/2D0+NUTF*RQQ(3,1)/4D0
     .      +NDTF*RQQ(3,2)/4D0+0.5D0*NUTF*(NUTF-1)*RQQ(3,3)
     .      +0.5D0*NDTF*(NDTF-1)*RQQ(3,4)+NUTF*NDTF*RQQ(3,5))*FACTOR
      EETOT=EEGG+EEGZ+EEZZ
      END
C
C**********************************************************************
C SFEE5J CALCULATES THE PROCESS E+ E- -> G,Z -> Q + QB + ( GGG OR QQBG)
C INCLUDES: SPIN AVERAGES, FINAL STATE STATISTICS, COUPLINGS
C**********************************************************************
      SUBROUTINE SFEE5J(EETOT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /GENERA/ W,Q,QCDL,EM,GS,SW2,RMZ,RGZ,NJET,NF,NUTF,NDTF,ICON
      COMMON /RESULT/ EEGG,EEGZ,EEZZ
      DIMENSION RGG(3,2),RQQ(3,5)
C
      CALL EEQ2G3(1,2,3,4,5,6,7,RGG(1,1),SW2,RMZ,RGZ)
      CALL EEQ4G(1,2,3,4,5,6,7,RQQ(1,1),SW2,RMZ,RGZ)
C
      FACTOR=EM**4*GS**6/4D0
C
      EEGG=(+NUTF*RGG(1,1)/6D0+NDTF*RGG(1,2)/6D0+NUTF*RQQ(1,1)/4D0
     .      +NDTF*RQQ(1,2)/4D0+0.5D0*NUTF*(NUTF-1)*RQQ(1,3)
     .      +0.5D0*NDTF*(NDTF-1)*RQQ(1,4)+NUTF*NDTF*RQQ(1,5))*FACTOR
      EEGZ=(+NUTF*RGG(2,1)/6D0+NDTF*RGG(2,2)/6D0+NUTF*RQQ(2,1)/4D0
     .      +NDTF*RQQ(2,2)/4D0+0.5D0*NUTF*(NUTF-1)*RQQ(2,3)
     .      +0.5D0*NDTF*(NDTF-1)*RQQ(2,4)+NUTF*NDTF*RQQ(2,5))*FACTOR
      EEZZ=(+NUTF*RGG(3,1)/6D0+NDTF*RGG(3,2)/6D0+NUTF*RQQ(3,1)/4D0
     .      +NDTF*RQQ(3,2)/4D0+0.5D0*NUTF*(NUTF-1)*RQQ(3,3)
     .      +0.5D0*NDTF*(NDTF-1)*RQQ(3,4)+NUTF*NDTF*RQQ(3,5))*FACTOR
      EETOT=EEGG+EEGZ+EEZZ
      END
C
C**********************************************************************
C MATRIX ELEMENT FOR:  E+ E- --> G,Z --> Q QB + N GLUONS
C**********************************************************************
C INFORMATION: THE PARTICLES IN PLAB ARE ORDERED AS FOLLOWS:
C             1,2 INCOMING PARTICLES 1: E+ AND 2: E-
C             3,4 QUARK AND ANTI-QUARK
C             5,.., 4+N GLUONS IN FINAL STATE.
C**********************************************************************
C   OUTPUT: THE MATRIX ELEMENTS FOR THE SUBPROCESSES RETURNED IN
C   R(3,2): FIRST INDEX ARE MIXINGS ==> 1: GG  2: GZ  3: ZZ
C           SECOND INDEX ARE FLAVOURS ==> 1: U-TYPE, 2: D-TYPE.
C   NOT INCLUDED: STRONG COUPLING CONSTANTS!
C                 STATISTICS, SPIN AVERAGES.
C**********************************************************************
C CALLING PROCEDURE FSEEGZ:
C             1       : ANTI-LEPTON (E+ INCOMING)
C             2       : LEPTON (E- INCOMING)
C             3,4     : LABELS THE QUARK AND THE ANTI-QUARK IN PLAB
C             5 .. 4+N: LABELS OF GLUONS IN PLAB ARRAY
C             SW2     : SIN(WEAK ANGLE)**2
C             RMZ     : MASS OF Z-BOSON
C             RGZ     : WIDTH OF Z-BOSON
C**********************************************************************
C MOST OF THE FORMULAE IN THIS PROGRAM CAN BE FOUND IN:
C   EXACT EXPRESSIONS FOR PROCESSES INVOLVING A VECTOR BOSON
C   AND UP TO FIVE PARTONS. (BERENDS, GIELE, KUIJF)
C   TO BE PUBLISHED IN NULCEAR PHYSICS B
C**********************************************************************
      SUBROUTINE EEQ2G0(I1,I2,I3,I4,R,SW2,RMZ,RGZ)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      COMMON /MOM/ PLAB(4,10)
      COMMON /KINEMA/ P(0:3,1:7)
      DIMENSION R(3,2),ISGN(7)
      DATA ISGN/-1,-1,1,1,1,1,1/
C
C TRANSFORM VECTORS
      DO 10 MU=0,3
        NU=MU
        IF(MU.EQ.0) NU=4
        P(MU,1)=ISGN(I3)*PLAB(NU,I3)
        P(MU,2)=ISGN(I4)*PLAB(NU,I4)
        P(MU,6)=ISGN(I1)*PLAB(NU,I1)
        P(MU,7)=ISGN(I2)*PLAB(NU,I2)
   10 CONTINUE
      CALL FSEEGZ(0,R,SW2,RMZ,RGZ)
      END
C
      SUBROUTINE EEQ2G1(I1,I2,I3,I4,I5,R,SW2,RMZ,RGZ)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      COMMON /MOM/ PLAB(4,10)
      COMMON /KINEMA/ P(0:3,1:7)
      DIMENSION R(3,2),ISGN(7)
      DATA ISGN/-1,-1,1,1,1,1,1/
C
C TRANSFORM VECTORS
      DO 10 MU=0,3
        NU=MU
        IF(MU.EQ.0) NU=4
        P(MU,1)=ISGN(I3)*PLAB(NU,I3)
        P(MU,2)=ISGN(I4)*PLAB(NU,I4)
        P(MU,3)=ISGN(I5)*PLAB(NU,I5)
        P(MU,6)=ISGN(I1)*PLAB(NU,I1)
        P(MU,7)=ISGN(I2)*PLAB(NU,I2)
   10 CONTINUE
      CALL FSEEGZ(1,R,SW2,RMZ,RGZ)
      END
C
      SUBROUTINE EEQ2G2(I1,I2,I3,I4,I5,I6,R,SW2,RMZ,RGZ)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      COMMON /MOM/ PLAB(4,10)
      COMMON /KINEMA/ P(0:3,1:7)
      DIMENSION R(3,2),ISGN(7)
      DATA ISGN/-1,-1,1,1,1,1,1/
C
C TRANSFORM VECTORS
      DO 10 MU=0,3
        NU=MU
        IF(MU.EQ.0) NU=4
        P(MU,1)=ISGN(I3)*PLAB(NU,I3)
        P(MU,2)=ISGN(I4)*PLAB(NU,I4)
        P(MU,3)=ISGN(I5)*PLAB(NU,I5)
        P(MU,4)=ISGN(I6)*PLAB(NU,I6)
        P(MU,6)=ISGN(I1)*PLAB(NU,I1)
        P(MU,7)=ISGN(I2)*PLAB(NU,I2)
   10 CONTINUE
      CALL FSEEGZ(2,R,SW2,RMZ,RGZ)
      END
C
      SUBROUTINE EEQ2G3(I1,I2,I3,I4,I5,I6,I7,R,SW2,RMZ,RGZ)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      COMMON /MOM/ PLAB(4,10)
      COMMON /KINEMA/ P(0:3,1:7)
      DIMENSION R(3,2),ISGN(7)
      DATA ISGN/-1,-1,1,1,1,1,1/
C
C TRANSFORM VECTORS
      DO 10 MU=0,3
        NU=MU
        IF(MU.EQ.0) NU=4
        P(MU,1)=ISGN(I3)*PLAB(NU,I3)
        P(MU,2)=ISGN(I4)*PLAB(NU,I4)
        P(MU,3)=ISGN(I5)*PLAB(NU,I5)
        P(MU,4)=ISGN(I6)*PLAB(NU,I6)
        P(MU,5)=ISGN(I7)*PLAB(NU,I7)
        P(MU,6)=ISGN(I1)*PLAB(NU,I1)
        P(MU,7)=ISGN(I2)*PLAB(NU,I2)
   10 CONTINUE
      CALL FSEEGZ(3,R,SW2,RMZ,RGZ)
      END
C
C**********************************************************************
C FUNCTION FSEEGZ(N,R,SW2,RMZ,RGZ) CALCULATES THE MATRIX ELEMENT
C SQUARED OF: E+ E- --> G,Z --> Q + QB + N GLUONS
C THE INPUT VECTORS ARE IN /KINEMA/ P(0:3,1:7), WHERE THE SIXTH
C AND SEVENTH VECTORS ARE THE E- AND THE E+ OUTGOING!.
C THE FIRST AND SECOND ARE THE QUARK AND THE ANTI-QUARK.
C**********************************************************************
      SUBROUTINE FSEEGZ(N,R,SW2,RMZ,RGZ)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION R(3,2)
      COMMON /SPINOR/ QLAD(1:2),QRB(1:2)
      COMPLEX*16 QLAD,QRB,ZRSADB(2,2,6,8),ZLSADB(2,2,6,8)
      DIMENSION ZGR(2,2),ZGL(2,2),ZZR(2,2),ZZL(2,2)
      COMMON /ESADB/ ZSADB(2,2,6,8)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      DIMENSION ZGRR(6,8),ZGRL(6,8),ZGLR(6,8),ZGLL(6,8),
     .          ZZRR(6,8),ZZRL(6,8),ZZLR(6,8),ZZLL(6,8)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      PARAMETER(C2A=16D0/3D0,C2B=-2D0/3D0,
     .          C3A=64D0/9D0,C3B=-8D0/9D0,C3C=1D0/9D0,C3D=10D0/9D0)
      DIMENSION COL(6,6,0:3)
      DATA COL / 3D0,35*0D0,
     .           4D0,35*0D0,
     .           C2A,C2B,4*0D0,C2B,C2A,28*0D0,
     .           C3A,C3B,C3B,C3C,C3C,C3D,C3B,C3A,C3C,C3D,C3B,C3C,
     .           C3B,C3C,C3A,C3B,C3D,C3C,C3C,C3D,C3B,C3A,C3C,C3B,
     .           C3C,C3B,C3D,C3C,C3A,C3B,C3D,C3C,C3C,C3B,C3B,C3A/
C
C INIT THE INPRODUCTS.
      DO 101 I=1,1+N
        DO 101 J=I,2+N
          RIN(I,J)=RINPRO(I,J)
          RIN(J,I)=RIN(I,J)
  101 CONTINUE
C
C INITIALIZED THE SPINORS AND THEIR INPRODUCTS
      CALL SETSPN(2+N,2)
C
C RENAME THE QUARK SPINOR IS QL(EFT) DOTTED AND QR(IGHT) UNDOTTED.
      QLAD(1)=ZKOD(1,1)
      QLAD(2)=ZKOD(1,2)
      QRB(1)=ZKO(2,1)
      QRB(2)=ZKO(2,2)
C
C INIT THE COUPLINGS AND PROPAGATORS.
C USE THE ENERGY DEPENDENT WIDTH FOR THE Z
      CW=DSQRT(1D0-SW2)
      SCW=DSQRT(SW2)*CW
      S=2D0*RINPRO(6,7)
      ZPRG=(0D0,-1D0)/S
      ZPRZ=(0D0,-1D0)/(S-RMZ**2+(0D0,1D0)*RGZ*S/RMZ)
      CGLLEP=1D0
      CGRLEP=1D0
      CGUR=-2D0/3D0
      CGDR=+1D0/3D0
      CGUL=-2D0/3D0
      CGDL=+1D0/3D0
      CZLLEP=(-0.5D0+SW2)/SCW
      CZRLEP=SW2/SCW
      CZUL=(+0.5D0-SW2*2D0/3D0)/SCW
      CZUR=(-SW2*2D0/3D0)/SCW
      CZDL=(-0.5D0+SW2*1D0/3D0)/SCW
      CZDR=(+SW2*1D0/3D0)/SCW
C
C INIT THE LEPTON CURRENTS
      DO 5 IX=1,2
        DO 5 IY=1,2
          ZGL(IX,IY)=ZKOD(7,IX)*ZKO(6,IY)*CGLLEP*ZPRG
          ZGR(IX,IY)=ZKOD(6,IX)*ZKO(7,IY)*CGRLEP*ZPRG
          ZZL(IX,IY)=ZKOD(7,IX)*ZKO(6,IY)*CZLLEP*ZPRZ
          ZZR(IX,IY)=ZKOD(6,IX)*ZKO(7,IY)*CZRLEP*ZPRZ
    5 CONTINUE
C
      IF (N.EQ.0) THEN
        FACTOR=1D0
        DO 6 I=1,2
          DO 6 J=1,2
            ZRSADB(I,J,1,1)=QLAD(I)*QRB(J)
            ZLSADB(I,J,1,1)=DCONJG(ZRSADB(I,J,1,1))
    6   CONTINUE
        IHEL=1
        IPERM=1
      END IF
C
      IF (N.EQ.1) THEN
        FACTOR=2D0
        DO 7 I=1,2
          DO 7 J=1,2
            ZRSADB(I,J,1,1)=+(QLAD(I)*ZUD(1,2)
     .        +ZKOD(3,I)*ZUD(3,2))*QRB(J)/(ZUD(1,3)*ZUD(3,2))
            ZLSADB(I,J,1,1)=DCONJG(ZRSADB(I,J,1,1))
            ZRSADB(I,J,1,2)=+QLAD(I)*(ZKO(3,J)*ZD(3,1)
     .        +QRB(J)*ZD(2,1))/(ZD(1,3)*ZD(3,2))
            ZLSADB(I,J,1,2)=DCONJG(ZRSADB(I,J,1,2))
    7   CONTINUE
        IHEL=2
        IPERM=1
      END IF
C
      IF (N.EQ.2) THEN
        FACTOR=4D0
        CALL F2SADB(3,4,1,1,2,3,4)
        CALL F2SADB(4,3,2,1,3,2,4)
        DO 8 I=1,4
          DO 8 J=1,2
            DO 8 IX=1,2
              DO 8 IY=1,2
                ZRSADB(IX,IY,J,I)=ZSADB(IX,IY,J,I)
                ZLSADB(IX,IY,J,I)=DCONJG(ZSADB(IX,IY,J,I))
    8   CONTINUE
        IHEL=4
        IPERM=2
      END IF
C
      IF (N.EQ.3) THEN
        FACTOR=8D0
        CALL F3SADB(3,4,5,1,1,2,3,4,5,6,7,8)
        CALL F3SADB(3,5,4,2,1,3,2,4,5,7,6,8)
        CALL F3SADB(4,3,5,3,1,2,4,3,6,5,7,8)
        CALL F3SADB(4,5,3,4,1,4,2,3,6,7,5,8)
        CALL F3SADB(5,3,4,5,1,3,4,2,7,5,6,8)
        CALL F3SADB(5,4,3,6,1,4,3,2,7,6,5,8)
        DO 9 I=1,8
          DO 9 J=1,6
            DO 9 IX=1,2
              DO 9 IY=1,2
                ZRSADB(IX,IY,J,I)=ZSADB(IX,IY,J,I)
                ZLSADB(IX,IY,J,I)=DCONJG(ZSADB(IX,IY,J,I))
    9   CONTINUE
        IHEL=8
        IPERM=6
      END IF
C
      DO 10 I=1,IHEL
        DO 10 J=1,IPERM
          ZGRR(J,I)=ZSPV(ZRSADB(1,1,J,I),ZGR(1,1))
          ZGRL(J,I)=ZSPV(ZLSADB(1,1,J,I),ZGR(1,1))
          ZGLR(J,I)=ZSPV(ZRSADB(1,1,J,I),ZGL(1,1))
          ZGLL(J,I)=ZSPV(ZLSADB(1,1,J,I),ZGL(1,1))
          ZZRR(J,I)=ZSPV(ZRSADB(1,1,J,I),ZZR(1,1))
          ZZRL(J,I)=ZSPV(ZLSADB(1,1,J,I),ZZR(1,1))
          ZZLR(J,I)=ZSPV(ZRSADB(1,1,J,I),ZZL(1,1))
          ZZLL(J,I)=ZSPV(ZLSADB(1,1,J,I),ZZL(1,1))
   10 CONTINUE
C
      DO 15 I=1,3
        DO 15 J=1,2
   15     R(I,J)=0D0
C
      DO 20 I=1,IHEL
        DO 20 J1=1,IPERM
          DO 20 J2=1,IPERM
            R(1,1)=R(1,1)+FACTOR*COL(J1,J2,N)*(
     .            +ZGRR(J1,I)*DCONJG(ZGRR(J2,I))*CGUR*CGUR
     .            +ZGRL(J1,I)*DCONJG(ZGRL(J2,I))*CGUL*CGUL
     .            +ZGLR(J1,I)*DCONJG(ZGLR(J2,I))*CGUR*CGUR
     .            +ZGLL(J1,I)*DCONJG(ZGLL(J2,I))*CGUL*CGUL )
            ZZ=FACTOR*COL(J1,J2,N)*(
     .            +ZZRR(J1,I)*DCONJG(ZGRR(J2,I))*CZUR*CGUR
     .            +ZZRL(J1,I)*DCONJG(ZGRL(J2,I))*CZUL*CGUL
     .            +ZZLR(J1,I)*DCONJG(ZGLR(J2,I))*CZUR*CGUR
     .            +ZZLL(J1,I)*DCONJG(ZGLL(J2,I))*CZUL*CGUL )
            R(2,1)=R(2,1)+2D0*DREAL(ZZ)
            R(3,1)=R(3,1)+FACTOR*COL(J1,J2,N)*(
     .            +ZZRR(J1,I)*DCONJG(ZZRR(J2,I))*CZUR*CZUR
     .            +ZZRL(J1,I)*DCONJG(ZZRL(J2,I))*CZUL*CZUL
     .            +ZZLR(J1,I)*DCONJG(ZZLR(J2,I))*CZUR*CZUR
     .            +ZZLL(J1,I)*DCONJG(ZZLL(J2,I))*CZUL*CZUL )
            R(1,2)=R(1,2)+FACTOR*COL(J1,J2,N)*(
     .            +ZGRR(J1,I)*DCONJG(ZGRR(J2,I))*CGDR*CGDR
     .            +ZGRL(J1,I)*DCONJG(ZGRL(J2,I))*CGDL*CGDL
     .            +ZGLR(J1,I)*DCONJG(ZGLR(J2,I))*CGDR*CGDR
     .            +ZGLL(J1,I)*DCONJG(ZGLL(J2,I))*CGDL*CGDL )
            ZZ=FACTOR*COL(J1,J2,N)*(
     .            +ZZRR(J1,I)*DCONJG(ZGRR(J2,I))*CZDR*CGDR
     .            +ZZRL(J1,I)*DCONJG(ZGRL(J2,I))*CZDL*CGDL
     .            +ZZLR(J1,I)*DCONJG(ZGLR(J2,I))*CZDR*CGDR
     .            +ZZLL(J1,I)*DCONJG(ZGLL(J2,I))*CZDL*CGDL )
            R(2,2)=R(2,2)+2D0*DREAL(ZZ)
            R(3,2)=R(3,2)+FACTOR*COL(J1,J2,N)*(
     .            +ZZRR(J1,I)*DCONJG(ZZRR(J2,I))*CZDR*CZDR
     .            +ZZRL(J1,I)*DCONJG(ZZRL(J2,I))*CZDL*CZDL
     .            +ZZLR(J1,I)*DCONJG(ZZLR(J2,I))*CZDR*CZDR
     .            +ZZLL(J1,I)*DCONJG(ZZLL(J2,I))*CZDL*CZDL )
   20 CONTINUE
C
      END
C
C**********************************************************************
C SUBROUTINE F2SADB(I1,I2,INDEX,J1,J2,J3,J4) CALCULATES THE FOUR
C HELICITY AMPLITUDES FOR THE PERMUTATION OF THE GLUONS (I1,I2).
C THE QUARK(Q) HAVING + HELICITY AND THE ANTI-QUARK(P)  - HELICTITY.
C THE CALCULATION IS ORGANIZED AS FOLLOWS:
C AFTER CALCULATING THE VARIOUS PROPAGATORS, ALL THE SPINOR
C CURRENTS FROM THE FORM: ( PROPAGATOR CONTRACTED WITH A SPINOR)
C ARE COMPOSED. THE CURRENTS CONTAINING THE Q ARE DOTTED. THOSE WITH
C THE QB ARE UNDOTTED.
C EXAMPLE: ZQ12M2 IS THE SPINOR (Q+G(I1)+G(I2))ADB * G(I2)B
C AFTER INITIALIZING VARIOUS CONSTANTS BELONGING TO A CERTAIN HELICITY
C COMBINATION, E.G. ZPPX BELONGS TO THE COMBINATION WHERE BOTH
C GLUONS HAVE HELICITY +, THE CURRENTS ZSADB ARE CALCULATED.
C**********************************************************************
C CALLING CONVENTIONS:
C   I1,I2 = PERMUTATION OF THE GLUONS. BECAUSE THEY ARE USED
C           AS INDICES WE ADDED TWO.
C           THIS WAY WE HAVE (I1,I2)= (3,4) OR (4,3)
C   INDEX = PERMUTATION COUNTER
C   J1,J2,J3,J4 = INDICES TO GET THE HELICITY CURRENTS ON THE
C                 RIGHT SPOT IN THE DATA STRUCTURES SADB.
C ALSO NEEDED ARE COMMONS /SPINOR/ AND /DOTPR/
C**********************************************************************
C OUTPUT CONVENTIONS:
C   PUT ALL THE HELICITY COMBINATIONS OF A CERTAIN
C   PERMUTATION (I1,I2) IN THE DATA-STRUCTURE ZSADB(2,2,6,8)
C**********************************************************************
      SUBROUTINE F2SADB(I1,I2,INDEX,J1,J2,J3,J4)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      COMMON /SPINOR/ QLAD(1:2),QRB(1:2)
      COMPLEX*16 QLAD,QRB
      COMMON /ESADB/ ZSADB(2,2,6,8)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION ZQ12M1(2),ZQ12M2(2),ZQ12MP(2),
     .          Z12PM1(2),Z12PM2(2),Z12PMQ(2)
C
      PRO12=2D0*RIN(I1,I2)
      PROQ12=PRO12+2D0*(RIN(1,I1)+RIN(1,I2))
      PRO12P=PRO12+2D0*(RIN(2,I1)+RIN(2,I2))
C
      DO 10 I=1,2
        ZQ12M1(I)=QLAD(I)*ZUD(1,I1)+ZKOD(I2,I)*ZUD(I2,I1)
        ZQ12M2(I)=QLAD(I)*ZUD(1,I2)+ZKOD(I1,I)*ZUD(I1,I2)
        ZQ12MP(I)=QLAD(I)*ZUD(1,2)+ZKOD(I1,I)*ZUD(I1,2)
     .                            +ZKOD(I2,I)*ZUD(I2,2)
        Z12PM1(I)=QRB(I)*ZD(2,I1)+ZKO(I2,I)*ZD(I2,I1)
        Z12PM2(I)=QRB(I)*ZD(2,I2)+ZKO(I1,I)*ZD(I1,I2)
        Z12PMQ(I)=QRB(I)*ZD(2,1)+ZKO(I1,I)*ZD(I1,1)+ZKO(I2,I)*ZD(I2,1)
   10 CONTINUE
C
      ZPPA=-1D0/(ZUD(1,I1)*ZUD(I1,I2)*ZUD(I2,2))
      ZPMA=ZD(1,I1)*ZUD(1,I2)/(PRO12*PROQ12*ZUD(1,I1))
      ZPMB=ZD(2,I1)*ZUD(2,I2)/(PRO12*PRO12P*ZD(2,I2))
      ZPMC=1D0/(PRO12*ZUD(1,I1)*ZD(I2,2))
      ZMPA=ZUD(2,I1)**2/(PRO12*PRO12P*ZUD(2,I2))
      ZMPB=ZD(1,I2)**2/(PRO12*PROQ12*ZD(1,I1))
      ZMPC=-ZD(1,I2)*ZUD(2,I1)/(PRO12*ZD(1,I1)*ZUD(2,I2))
      ZMMA=1D0/(ZD(1,I1)*ZD(I1,I2)*ZD(I2,2))
C
      DO 20 IX=1,2
        DO 20 IY=1,2
          ZSADB(IX,IY,INDEX,J1)=+ZPPA*ZQ12MP(IX)*QRB(IY)
          ZSADB(IX,IY,INDEX,J2)=+ZPMA*ZQ12M2(IX)*QRB(IY)
     .                          +ZPMB*QLAD(IX)*Z12PM1(IY)
     .                          +ZPMC*ZQ12M2(IX)*Z12PM1(IY)
          ZSADB(IX,IY,INDEX,J3)=+ZMPA*QLAD(IX)*Z12PM2(IY)
     .                          +ZMPB*ZQ12M1(IX)*QRB(IY)
     .                          +ZMPC*QLAD(IX)*QRB(IY)
          ZSADB(IX,IY,INDEX,J4)=+ZMMA*QLAD(IX)*Z12PMQ(IY)
   20 CONTINUE
C
      END
C
C**********************************************************************
C SUBROUTINE F3SADB(I1,I2,I3,INDEX,J1,J2,J3,J4,J5,J6,J7,J8)
C CALCULATES THE EIGHT HELICITY AMPLITUDES FOR THE PERMUTATION
C OF THE GLUONS (I1,I2,I3).
C THE QUARK(Q) HAVING + HELICITY AND THE ANTI-QUARK(P)  - HELICTITY.
C THE CALCULATIONS OF THE ZSADB'S IS COMPLETELY ANALOGOUS TO THE TWO
C GLUON CASE.
C**********************************************************************
C CALLING CONVENTIONS:
C   I1,I2,I3 = PERMUTATION OF THE GLUONS. E.G. (I1,I2,I3)= (3,4,5)
C   INDEX = PERMUTATION COUNTER
C   J1,J2,J3,J4 = INDICES TO GET THE HELICITY CURRENTS ON THE
C   J5,J6,J7,J8   RIGHT SPOT IN THE DATA STRUCTURES ZSADB.
C ALSO NEEDED ARE COMMONS /SPINOR/ AND /DOTPR/
C**********************************************************************
C OUTPUT CONVENTIONS:
C   PUT ALL THE HELICITY COMBINATIONS OF A CERTAIN
C   PERMUTATION (I1,I2,I3) IN THE DATA-STRUCTURE ZSADB(2,2,6,8)
C**********************************************************************
      SUBROUTINE F3SADB(I1,I2,I3,INDEX,J1,J2,J3,J4,J5,J6,J7,J8)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      COMMON /SPINOR/ QLAD(1:2),QRB(1:2)
      COMPLEX*16 QLAD,QRB
      COMMON /ESADB/ ZSADB(2,2,6,8)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION ZQ12M1(2),ZQ12M2(2),ZQ12MP(2),ZQ13M1(2),ZQ13M2(2),
     .          ZQ13M3(2),ZQ13MP(2),Z23PM3(2),Z23PM2(2),Z23PMQ(2),
     .          Z13PM1(2),Z13PM2(2),Z13PM3(2),Z13PMQ(2)
C
      PRO12=2D0*RIN(I1,I2)
      PRO23=2D0*RIN(I2,I3)
      PRO13=PRO12+PRO23+2D0*RIN(I1,I3)
      PROQ12=PRO12+2D0*(RIN(1,I1)+RIN(1,I2))
      PROQ13=PROQ12+PRO23+2D0*(RIN(1,I3)+RIN(I1,I3))
      PRO23P=PRO23+2D0*(RIN(2,I2)+RIN(2,I3))
      PRO13P=PRO23P+PRO12+2D0*(RIN(2,I1)+RIN(I1,I3))
      DO 10 I=1,2
        ZQ12M1(I)=QLAD(I)*ZUD(1,I1)+ZKOD(I2,I)*ZUD(I2,I1)
        ZQ12M2(I)=QLAD(I)*ZUD(1,I2)+ZKOD(I1,I)*ZUD(I1,I2)
        ZQ12MP(I)=QLAD(I)*ZUD(1,2)+ZKOD(I1,I)*ZUD(I1,2)
     .                            +ZKOD(I2,I)*ZUD(I2,2)
        ZQ13M1(I)=ZQ12M1(I)+ZKOD(I3,I)*ZUD(I3,I1)
        ZQ13M2(I)=ZQ12M2(I)+ZKOD(I3,I)*ZUD(I3,I2)
        ZQ13M3(I)=QLAD(I)*ZUD(1,I3)
     .               +ZKOD(I1,I)*ZUD(I1,I3)+ZKOD(I2,I)*ZUD(I2,I3)
        ZQ13MP(I)=ZQ12MP(I)+ZKOD(I3,I)*ZUD(I3,2)
        Z23PM2(I)=QRB(I)*ZD(2,I2)+ZKO(I3,I)*ZD(I3,I2)
        Z23PM3(I)=QRB(I)*ZD(2,I3)+ZKO(I2,I)*ZD(I2,I3)
        Z23PMQ(I)=QRB(I)*ZD(2,1)+ZKO(I2,I)*ZD(I2,1)+ZKO(I3,I)*ZD(I3,1)
        Z13PM1(I)=QRB(I)*ZD(2,I1)+ZKO(I2,I)*ZD(I2,I1)
     .                           +ZKO(I3,I)*ZD(I3,I1)
        Z13PM2(I)=Z23PM2(I)+ZKO(I1,I)*ZD(I1,I2)
        Z13PM3(I)=Z23PM3(I)+ZKO(I1,I)*ZD(I1,I3)
        Z13PMQ(I)=Z23PMQ(I)+ZKO(I1,I)*ZD(I1,1)
   10 CONTINUE
C
      ZPPPA=-1D0/(ZUD(1,I1)*ZUD(I1,I2)*ZUD(I2,I3)*ZUD(I3,2))
      ZPPMA=-ZD(I1,I2)*(ZD(1,I1)*ZUD(I3,I1)+ZD(1,I2)*ZUD(I3,I2))/
     .         (ZUD(I1,I2)*PRO23*PRO13*PROQ13)
     .      +(ZD(I2,1)*ZUD(I3,1)+ZD(I2,I1)*ZUD(I3,I1))/
     .         (ZUD(1,I1)*ZUD(I1,I2)*PRO23*PROQ13)
      ZPPMB=+ZUD(2,I3)*ZD(I1,I2)*ZUD(I3,I1)/
     .         (ZUD(I1,I2)*PRO23*PRO13*PRO13P)
     .      +ZUD(2,I3)**2*ZD(2,I2)/
     .         (ZUD(I1,I2)*PRO23*PRO23P*PRO13P)
      ZPPMC=+ZUD(2,I3)*ZD(I1,I2)*ZUD(I3,I2)/
     .         (ZUD(I1,I2)*PRO23*PRO13*PRO13P)
      ZPPMD=-ZUD(2,I3)*ZD(2,I2)/
     .         (ZUD(1,I1)*ZUD(I1,I2)*PRO23*ZD(I3,2)*PRO23P)
      ZPPME=1D0/(ZUD(1,I1)*ZUD(I1,I2)*PRO23*ZD(I3,2))
      ZPMPA=(-ZD(I1,I3)**2*(ZD(1,I1)*ZUD(I2,I1)+ZD(1,I3)*ZUD(I2,I3))/
     .         (PRO12*PRO23*PRO13)
     .       -ZD(1,I1)*ZUD(1,I2)*
     .         (ZD(I3,1)*ZUD(I2,1)+ZD(I3,I1)*ZUD(I2,I1))/
     .         (ZUD(1,I1)*PRO12*ZUD(I2,I3)*PROQ12)
     .       +ZD(I1,I3)*(ZD(I3,1)*ZUD(I2,1)+ZD(I3,I1)*ZUD(I2,I1))/
     .         (ZUD(1,I1)*PRO12*PRO23) )/PROQ13
      ZPMPB=(+ZUD(2,I2)**3*ZD(2,I3)/
     .         (ZUD(I1,I2)*PRO23*ZUD(I3,2)*PRO23P)
     .       -ZUD(I2,2)**2*ZD(I1,I3)/
     .         (PRO12*PRO23*ZUD(I3,2))
     .       -ZUD(I2,2)*ZD(I1,I3)**2*ZUD(I2,I1)/
     .         (PRO12*PRO23*PRO13)   )/PRO13P
      ZPMPC=-ZUD(I2,2)*ZD(I1,I3)**2*ZUD(I2,I3)/
     .         (PRO12*PRO23*PRO13*PRO13P)
      ZPMPD=-ZUD(1,I2)*ZUD(2,I2)*ZD(1,I1)/
     .         (ZUD(1,I1)*PRO12*ZUD(I2,I3)*ZUD(I3,2)*PROQ12)
      ZPMPE=-ZUD(2,I2)**2/(ZUD(1,I1)*ZUD(I1,I2)*PRO23*ZUD(I3,2)*PRO23P)
      ZPMPF=-ZD(I1,I3)*ZUD(I2,2)/(ZUD(1,I1)*PRO12*PRO23*ZUD(I3,2))
      ZMPPA=(-ZD(I2,I3)*(ZD(1,I2)*ZUD(I1,I2)+ZD(1,I3)*ZUD(I1,I3))/
     .         (PRO12*ZUD(I2,I3)*PRO13*PROQ13)
     .       +ZD(1,I2)*ZD(1,I3)/(ZD(1,I1)*PRO12*ZUD(I2,I3)*PROQ13))
      ZMPPB=(-ZD(1,I2)**2*(ZD(I3,1)*ZUD(I1,1)+ZD(I3,I2)*ZUD(I1,I2))/
     .         (ZD(1,I1)*PRO12*ZUD(I2,I3)*PROQ12*PROQ13)  )
      ZMPPC=+ZD(I2,I3)*ZUD(2,I1)*ZUD(I1,I2)/
     .         (PRO12*ZUD(I2,I3)*PRO13*PRO13P)
     .      -ZUD(2,I1)**2/(PRO12*ZUD(I2,I3)*ZUD(I3,2)*PRO13P)
      ZMPPD=+ZD(I2,I3)*ZUD(2,I1)*ZUD(I1,I3)/
     .         (PRO12*ZUD(I2,I3)*PRO13*PRO13P)
      ZMPPE=+ZUD(I2,2)*ZD(1,I2)**2/
     .         (ZD(1,I1)*PRO12*ZUD(I2,I3)*ZUD(I3,2)*PROQ12)
      ZMPPF=+ZUD(2,I1)*ZD(1,I2)/(ZD(1,I1)*PRO12*ZUD(I2,I3)*ZUD(I3,2))
      ZPMMA=+ZUD(I2,I3)*(ZD(I1,I2)*ZUD(2,I2)+ZD(I1,I3)*ZUD(2,I3))/
     .         (PRO12*ZD(I2,I3)*PRO13*PRO13P)
     .      -(ZD(I1,I3)*ZUD(I2,I3)+ZD(I1,2)*ZUD(I2,2))/
     .         (PRO12*ZD(I2,I3)*ZD(I3,2)*PRO13P)
      ZPMMB=-ZD(1,I1)*ZUD(I2,I3)*ZD(I1,I2)/
     .         (PRO12*ZD(I2,I3)*PRO13*PROQ13)
      ZPMMC=-ZD(1,I1)*ZUD(I2,I3)*ZD(I1,I3)/
     .         (PRO12*ZD(I2,I3)*PRO13*PROQ13)
     .      +ZD(1,I1)**2*ZUD(1,I2)/
     .         (PRO12*ZD(I2,I3)*PROQ12*PROQ13)
      ZPMMD=-ZD(1,I1)*ZUD(1,I2)/
     .         (ZUD(1,I1)*PRO12*ZD(I2,I3)*ZD(I3,2)*PROQ12)
      ZPMME=1D0/(ZUD(1,I1)*PRO12*ZD(I2,I3)*ZD(I3,2))
      ZMPMA=(+ZUD(I1,I3)**2*(ZD(I2,I1)*ZUD(2,I1)+ZD(I2,I3)*ZUD(2,I3))/
     .         (PRO12*PRO23*PRO13)
     .       +ZD(2,I2)*ZUD(2,I3)*
     .         (ZD(I2,I3)*ZUD(I1,I3)+ZD(I2,2)*ZUD(I1,2))/
     .         (ZD(I1,I2)*PRO23*ZD(I3,2)*PRO23P)
     .       -ZUD(I1,I3)*(ZD(I2,I3)*ZUD(I1,I3)+ZD(I2,2)*ZUD(I1,2))/
     .         (PRO12*PRO23*ZD(I3,2))   )/PRO13P
      ZMPMB=(-ZD(1,I2)**3*ZUD(1,I1)/
     .         (ZD(1,I1)*PRO12*ZD(I2,I3)*PROQ12)
     .       +ZD(1,I2)**2*ZUD(I1,I3)/
     .         (ZD(1,I1)*PRO12*PRO23)
     .       -ZD(1,I2)*ZUD(I1,I3)**2*ZD(I2,I3)/
     .         (PRO12*PRO23*PRO13)  )/PROQ13
      ZMPMC=-ZD(1,I2)*ZUD(I1,I3)**2*ZD(I2,I1)/
     .         (PRO12*PRO23*PRO13*PROQ13)
      ZMPMD=-ZD(2,I2)*ZD(1,I2)*ZUD(2,I3)/
     .         (ZD(1,I1)*ZD(I1,I2)*PRO23*ZD(I3,2)*PRO23P)
      ZMPME=-ZD(1,I2)**2/(ZD(1,I1)*PRO12*ZD(I2,I3)*ZD(I3,2)*PROQ12)
      ZMPMF=+ZUD(I1,I3)*ZD(1,I2)/(ZD(1,I1)*PRO12*PRO23*ZD(I3,2))
      ZMMPA=+ZUD(I1,I2)*(ZD(I3,I1)*ZUD(2,I1)+ZD(I3,I2)*ZUD(2,I2))/
     .         (ZD(I1,I2)*PRO23*PRO13*PRO13P)
     .      -ZUD(2,I1)*ZUD(2,I2)/
     .         (ZD(I1,I2)*PRO23*ZUD(I3,2)*PRO13P)
      ZMMPB=+ZUD(2,I2)**2*(ZD(I3,I2)*ZUD(I1,I2)+ZD(I3,2)*ZUD(I1,2))/
     .         (ZD(I1,I2)*PRO23*ZUD(I3,2)*PRO23P*PRO13P)
      ZMMPC=-ZUD(I1,I2)*ZD(1,I3)*ZD(I3,I1)/
     .         (ZD(I1,I2)*PRO23*PRO13*PROQ13)
      ZMMPD=-ZUD(I1,I2)*ZD(1,I3)*ZD(I3,I2)/
     .         (ZD(I1,I2)*PRO23*PRO13*PROQ13)
     .      +ZD(1,I3)**2/(ZD(1,I1)*ZD(I1,I2)*PRO23*PROQ13)
      ZMMPE=-ZUD(2,I2)**2*ZD(1,I2)/
     .         (ZD(1,I1)*ZD(I1,I2)*PRO23*ZUD(I3,2)*PRO23P)
      ZMMPF=ZUD(2,I2)*ZD(1,I3)/(ZD(1,I1)*ZD(I1,I2)*PRO23*ZUD(I3,2))
      ZMMMA=1D0/(ZD(1,I1)*ZD(I1,I2)*ZD(I2,I3)*ZD(I3,2))
C
      DO 20 IX=1,2
        DO 20 IY=1,2
          ZSADB(IX,IY,INDEX,J1)=+ZPPPA*ZQ13MP(IX)*QRB(IY)
          ZSADB(IX,IY,INDEX,J2)=+ZPPMA*ZQ13M3(IX)*QRB(IY)
     .                          +ZPPMB*QLAD(IX)*Z13PM1(IY)
     .                          +ZPPMC*QLAD(IX)*Z13PM2(IY)
     .                          +ZPPMD*ZQ12M2(IX)*Z23PM2(IY)
     .                          +ZPPME*ZQ13M3(IX)*Z23PM2(IY)
          ZSADB(IX,IY,INDEX,J3)=+ZPMPA*ZQ13M2(IX)*QRB(IY)
     .                          +ZPMPB*QLAD(IX)*Z13PM1(IY)
     .                          +ZPMPC*QLAD(IX)*Z13PM3(IY)
     .                          +ZPMPD*ZQ12M2(IX)*QRB(IY)
     .                          +ZPMPE*ZQ12M2(IX)*Z23PM3(IY)
     .                          +ZPMPF*ZQ12M2(IX)*QRB(IY)
          ZSADB(IX,IY,INDEX,J4)=+ZMPPA*ZQ13M1(IX)*QRB(IY)
     .                          +ZMPPB*ZQ13M2(IX)*QRB(IY)
     .                          +ZMPPC*QLAD(IX)*Z13PM2(IY)
     .                          +ZMPPD*QLAD(IX)*Z13PM3(IY)
     .                          +ZMPPE*ZQ12M1(IX)*QRB(IY)
     .                          +ZMPPF*QLAD(IX)*QRB(IY)
          ZSADB(IX,IY,INDEX,J5)=+ZPMMA*QLAD(IX)*Z13PM1(IY)
     .                          +ZPMMB*ZQ13M2(IX)*QRB(IY)
     .                          +ZPMMC*ZQ13M3(IX)*QRB(IY)
     .                          +ZPMMD*ZQ12M2(IX)*Z23PM2(IY)
     .                          +ZPMME*ZQ12M2(IX)*Z13PM1(IY)
          ZSADB(IX,IY,INDEX,J6)=+ZMPMA*QLAD(IX)*Z13PM2(IY)
     .                          +ZMPMB*ZQ13M3(IX)*QRB(IY)
     .                          +ZMPMC*ZQ13M1(IX)*QRB(IY)
     .                          +ZMPMD*QLAD(IX)*Z23PM2(IY)
     .                          +ZMPME*ZQ12M1(IX)*Z23PM2(IY)
     .                          +ZMPMF*QLAD(IX)*Z23PM2(IY)
          ZSADB(IX,IY,INDEX,J7)=+ZMMPA*QLAD(IX)*Z13PM3(IY)
     .                          +ZMMPB*QLAD(IX)*Z13PM2(IY)
     .                          +ZMMPC*ZQ13M1(IX)*QRB(IY)
     .                          +ZMMPD*ZQ13M2(IX)*QRB(IY)
     .                          +ZMMPE*QLAD(IX)*Z23PM3(IY)
     .                          +ZMMPF*QLAD(IX)*QRB(IY)
          ZSADB(IX,IY,INDEX,J8)=+ZMMMA*QLAD(IX)*Z13PMQ(IY)
   20 CONTINUE
C
      END
C
C**********************************************************************
C MATRIX ELEMENT FOR:  E+ E- --> G,Z --> Q QB Q QB
C**********************************************************************
C INFORMATION: THE PARTICLES IN PLAB ARE ORDERED AS FOLLOWS:
C                1,2 : INCOMING E+ AND E-
C                3,5 : OUTGOING QUARKS
C                4,6 : OUTGOING ANTI-QUARKS
C                SW2: SQUARE OF SIN(WEAK ANGLE)
C                RMZ: MASS OF Z
C                RGZ: WIDTH OF Z
C
C   OUTPUT: THE MATRIX ELEMENTS FOR THE SUBPROCESSES RETURNED IN
C   R(3,5): FIRST INDEX  1 = GG
C                        2 = GZ
C                        3 = ZZ
C           SECOND INDEX 1 = SAME QUARKS UP-TYPE
C                        2 = SAME QUARKS DOWN-TYPE
C                        3 = DIFFERENT QUARKS UP-TYPE
C                        4 = DIFFERENT QUARKS DOWN-TYPE
C                        5 = DIFFERENT QUARKS UP - DOWN PAIRS
C**********************************************************************
C   NOT INCLUDED: STRONG COUPLING CONSTANTS!
C                 STATISTICS, SPIN AVERAGES.
C**********************************************************************
C CALLING PROCEDURE FOR SUBROUTINES:
C             1  : ANTI-PARTICLE
C             2  : PARTICLE
C             3,5: LABELS OF QUARKS IN PLAB ARRAY
C             4,6: LABELS OF ANTI-QUARKS IN PLAB ARRAY
C PARTICLES 1 AND 2 ARE ALWAYS FLIPPED: WE CONSIDER ALL PARTICLE
C TO BE OUTGOING.
C**********************************************************************
C MOST OF THE FORMULAE IN THIS PROGRAM CAN BE FOUND IN:
C   EXACT EXPRESSIONS FOR PROCESSES INVOLVING A VECTOR BOSON
C   AND UP TO FIVE PARTONS. (BERENDS, GIELE, KUIJF)
C**********************************************************************
      SUBROUTINE EEQ4(I1,I2,I3,I4,I5,I6,R,SW2,RMZ,RGZ)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      COMMON /MOM/ PLAB(4,10)
      COMMON /KINEMA/ P(0:3,7)
      DIMENSION R(3,5),ISGN(7)
      DATA ISGN/-1,-1,1,1,1,1,1/
C
C TRANSFORM VECTORS
      DO 10 MU=0,3
        NU=MU
        IF(MU.EQ.0) NU=4
        P(MU,6)=ISGN(I1)*PLAB(NU,I1)
        P(MU,7)=ISGN(I2)*PLAB(NU,I2)
        P(MU,1)=ISGN(I3)*PLAB(NU,I3)
        P(MU,2)=ISGN(I4)*PLAB(NU,I4)
        P(MU,3)=ISGN(I5)*PLAB(NU,I5)
        P(MU,4)=ISGN(I6)*PLAB(NU,I6)
   10 CONTINUE
C
C CALCULATE KINEMATIC INVARIANTS
C
      CALL FILL
C
C INSERT DIFFERENT COUPLINGS TO OBTAIN SUBPROCESSES
C
      CALL SQUAR(1,1,SW2,RMZ,RGZ,R(1,1))
      CALL SQUAR(2,2,SW2,RMZ,RGZ,R(1,2))
      CALL SQUAR(1,3,SW2,RMZ,RGZ,R(1,3))
      CALL SQUAR(2,4,SW2,RMZ,RGZ,R(1,4))
      CALL SQUAR(1,2,SW2,RMZ,RGZ,R(1,5))
C
      END
C
C**********************************************************************
C FUNCTION SQUAR CALCULATES THE MATRIX ELEMENT SQUARED OF THE
C E+ E- --> G,Z --> Q + QB + Q + QB
C**********************************************************************
C DESCIPTION OF THE COMMON ZQQQQ
C     THE FIRST IS THE INDEX FOR THE SQUARED MATRIX
C     THE SECOND INDEX ARE THE DIFFERENT SUBAMPLITUDES (6)
C     THE FIRST AND THE SECOND ARE INDICES IN THE SQUARED MATRIX
C       INDEX 3=1 -->  ++--, =2 --> +--+, =3 --> -++-, =4 --> --++
C       INDEX 3=5 -->  +-+-, =6 --> -+-+
C**********************************************************************
C OUTPUT : R(3)   1: GG   2: GZ  3: ZZ
C**********************************************************************
      SUBROUTINE SQUAR(ITYP1,ITYP2,SW2,RMZ,RGZ,R)
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      PARAMETER(CUP=2D0/3D0,CDOWN=-1D0/3D0)
      PARAMETER(C1=8D0,C2=-8D0/3D0)
      COMMON /ZQQQQ/ ZR(4,6),ZL(4,6)
      DIMENSION S2G(4,2),S2Z(4,2),S4G(2,4),S4Z(2,4),CHARGE(4),CISOSP(4)
      DIMENSION COL4(4,4),COL2(2,2),R(3)
      DATA CHARGE/CUP,CDOWN,CUP,CDOWN/
      DATA CISOSP/0.5D0,-0.5D0,0.5D0,-0.5D0/
      DATA COL4/C1,C2,C1,C2,C2,C1,C2,C1,C1,C2,C1,C2,C2,C1,C2,C1/
      DATA COL2/C1,C1,C1,C1/
C
C INIT ANSWER
      R(1)=0D0
      R(2)=0D0
      R(3)=0D0
C
C COUPLINGS AND ALL THAT (LIKE ENERGY DEPENDENT WIDTH FOR Z PROPAGATOR).
      CW=DSQRT(1D0-SW2)
      SCW=DSQRT(SW2)*CW
      S=2D0*RINPRO(6,7)
      ZPRG=(0D0,-1D0)/S
      ZPRZ=(0D0,-1D0)/(S-RMZ**2+(0D0,1D0)*S*RGZ/RMZ)
      CGLLEP=1D0
      CGRLEP=1D0
      CG1R=-CHARGE(ITYP1)
      CG2R=-CHARGE(ITYP2)
      CG1L=-CHARGE(ITYP1)
      CG2L=-CHARGE(ITYP2)
      CZLLEP=(-0.5D0+SW2)/SCW
      CZRLEP=SW2/SCW
      CZ1L=(+CISOSP(ITYP1)-SW2*CHARGE(ITYP1))/SCW
      CZ1R=(-SW2*CHARGE(ITYP1))/SCW
      CZ2L=(+CISOSP(ITYP2)-SW2*CHARGE(ITYP2))/SCW
      CZ2R=(-SW2*CHARGE(ITYP2))/SCW
C
      IF(ITYP1.EQ.ITYP2)THEN
        DELTA=1D0
      ELSE
        DELTA=0D0
      END IF
C
C S2(1,.) IS PPMM
      S2G(1,1)=DELTA*CG1R
      S2Z(1,1)=DELTA*CZ1R
      S2G(1,2)=DELTA*CG1L
      S2Z(1,2)=DELTA*CZ1L
C S2(2,.) IS PMMP
      S2G(2,1)=CG1R
      S2Z(2,1)=CZ1R
      S2G(2,2)=CG2L
      S2Z(2,2)=CZ2L
C S2(3,.) IS MPPM
      S2G(3,1)=CG1L
      S2Z(3,1)=CZ1L
      S2G(3,2)=CG2R
      S2Z(3,2)=CZ2R
C S2(4,.) IS MMPP
      S2G(4,1)=DELTA*CG1L
      S2Z(4,1)=DELTA*CZ1L
      S2G(4,2)=DELTA*CG1R
      S2Z(4,2)=DELTA*CZ1R
C
      DO 10 I=1,4
        IJ=I
        DO 11 IX=1,2
          DO 12 IY=1,2
            R(1)=R(1)+COL2(IX,IY)*S2G(IJ,IX)*S2G(IJ,IY)*
     .           ZPRG*CGRLEP*ZR(IX,I)*DCONJG(CGRLEP*ZPRG*ZR(IY,I))
     .               +COL2(IX,IY)*S2G(IJ,IX)*S2G(IJ,IY)*
     .           ZPRG*CGLLEP*ZL(IX,I)*DCONJG(ZPRG*CGLLEP*ZL(IY,I))
            R(2)=R(2)+COL2(IX,IY)*S2G(IJ,IX)*S2Z(IJ,IY)*
     .           ZPRG*CGRLEP*ZR(IX,I)*DCONJG(ZPRZ*CZRLEP*ZR(IY,I))
     .               +COL2(IX,IY)*S2G(IJ,IX)*S2Z(IJ,IY)*
     .           ZPRG*CGLLEP*ZL(IX,I)*DCONJG(ZPRZ*CZLLEP*ZL(IY,I))
     .               +COL2(IX,IY)*S2Z(IJ,IX)*S2G(IJ,IY)*
     .           ZPRZ*CZRLEP*ZR(IX,I)*DCONJG(ZPRG*CGRLEP*ZR(IY,I))
     .               +COL2(IX,IY)*S2Z(IJ,IX)*S2G(IJ,IY)*
     .           ZPRZ*CZLLEP*ZL(IX,I)*DCONJG(ZPRG*CGLLEP*ZL(IY,I))
            R(3)=R(3)+COL2(IX,IY)*S2Z(IJ,IX)*S2Z(IJ,IY)*
     .           ZPRZ*CZRLEP*ZR(IX,I)*DCONJG(ZPRZ*CZRLEP*ZR(IY,I))
     .               +COL2(IX,IY)*S2Z(IJ,IX)*S2Z(IJ,IY)*
     .           ZPRZ*CZLLEP*ZL(IX,I)*DCONJG(ZPRZ*CZLLEP*ZL(IY,I))
   12     CONTINUE
   11   CONTINUE
   10 CONTINUE
C
C S4(1,.) IS PMPM
      S4G(1,1)=CG1R
      S4Z(1,1)=CZ1R
      S4G(1,2)=DELTA*CG1R
      S4Z(1,2)=DELTA*CZ1R
      S4G(1,3)=CG2R
      S4Z(1,3)=CZ2R
      S4G(1,4)=DELTA*CG1R
      S4Z(1,4)=DELTA*CZ1R
C S4(2,.) IS MPMP
      S4G(2,1)=CG1L
      S4Z(2,1)=CZ1L
      S4G(2,2)=DELTA*CG1L
      S4Z(2,2)=DELTA*CZ1L
      S4G(2,3)=CG2L
      S4Z(2,3)=CZ2L
      S4G(2,4)=DELTA*CG1L
      S4Z(2,4)=DELTA*CZ1L
C
      DO 20 I=5,6
        IJ=(I-4)
        DO 21 IX=1,4
          DO 22 IY=1,4
            R(1)=R(1)+COL4(IX,IY)*S4G(IJ,IX)*S4G(IJ,IY)*
     .           ZPRG*CGRLEP*ZR(IX,I)*DCONJG(CGRLEP*ZPRG*ZR(IY,I))
     .               +COL4(IX,IY)*S4G(IJ,IX)*S4G(IJ,IY)*
     .           ZPRG*CGLLEP*ZL(IX,I)*DCONJG(ZPRG*CGLLEP*ZL(IY,I))
            R(2)=R(2)+COL4(IX,IY)*S4G(IJ,IX)*S4Z(IJ,IY)*
     .           ZPRG*CGRLEP*ZR(IX,I)*DCONJG(ZPRZ*CZRLEP*ZR(IY,I))
     .               +COL4(IX,IY)*S4G(IJ,IX)*S4Z(IJ,IY)*
     .           ZPRG*CGLLEP*ZL(IX,I)*DCONJG(ZPRZ*CZLLEP*ZL(IY,I))
     .               +COL4(IX,IY)*S4Z(IJ,IX)*S4G(IJ,IY)*
     .           ZPRZ*CZRLEP*ZR(IX,I)*DCONJG(ZPRG*CGRLEP*ZR(IY,I))
     .               +COL4(IX,IY)*S4Z(IJ,IX)*S4G(IJ,IY)*
     .           ZPRZ*CZLLEP*ZL(IX,I)*DCONJG(ZPRG*CGLLEP*ZL(IY,I))
            R(3)=R(3)+COL4(IX,IY)*S4Z(IJ,IX)*S4Z(IJ,IY)*
     .           ZPRZ*CZRLEP*ZR(IX,I)*DCONJG(ZPRZ*CZRLEP*ZR(IY,I))
     .               +COL4(IX,IY)*S4Z(IJ,IX)*S4Z(IJ,IY)*
     .           ZPRZ*CZLLEP*ZL(IX,I)*DCONJG(ZPRZ*CZLLEP*ZL(IY,I))
   22     CONTINUE
   21   CONTINUE
   20 CONTINUE
C
      END
 
C**********************************************************************
C SUBROUTINE FILL CALCULATES THE VARIOUS INVARIANTS FOR Q QB Q QB
C    THE PROBLEM OF AN ODD NUMBER OF QUARK-PARTICLES WITH NEGATIVE
C    ENERGY IS HANDLED WITHIN THE H-FUNCTIONS.
C**********************************************************************
      SUBROUTINE FILL()
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      COMMON /ZQQQQ/ ZR(4,6),ZL(4,6)
      DIMENSION ZLCUR(2,2),ZRCUR(2,2),ZA(2,2),ZB(2,2)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
C
C THE 4 VECTORS MUST BE IN P(MU,1) .. P(MU,4)
      CALL SETSPN(4,4)
C
      DO 5 I=1,3
        DO 5 J=I+1,4
          RIN(I,J)=RINPRO(I,J)
          RIN(J,I)=RIN(I,J)
    5 CONTINUE
C
      DO 6 IX=1,2
        DO 6 IY=1,2
          ZRCUR(IX,IY)=ZKOD(6,IX)*ZKO(7,IY)
          ZLCUR(IX,IY)=ZKOD(7,IX)*ZKO(6,IY)
    6 CONTINUE
C
C HANDLE PPMM AND MMPP
      CALL HPMMP(1,4,3,2,-1D0,ZA(1,1),ZB(1,1))
      ZR(1,1)=ZSPV(ZRCUR(1,1),ZA(1,1))
      ZL(1,1)=ZSPV(ZLCUR(1,1),ZA(1,1))
      ZR(1,4)=ZSPV(ZRCUR(1,1),ZB(1,1))
      ZL(1,4)=ZSPV(ZLCUR(1,1),ZB(1,1))
      CALL HPMMP(3,2,1,4,-1D0,ZA(1,1),ZB(1,1))
      ZR(2,4)=ZSPV(ZRCUR(1,1),ZA(1,1))
      ZL(2,4)=ZSPV(ZLCUR(1,1),ZA(1,1))
      ZR(2,1)=ZSPV(ZRCUR(1,1),ZB(1,1))
      ZL(2,1)=ZSPV(ZLCUR(1,1),ZB(1,1))
C
C HANDLE PMMP AND MPPM
      CALL HPMMP(1,2,3,4,1D0,ZA(1,1),ZB(1,1))
      ZR(1,2)=ZSPV(ZRCUR(1,1),ZA(1,1))
      ZL(1,2)=ZSPV(ZLCUR(1,1),ZA(1,1))
      ZR(1,3)=ZSPV(ZRCUR(1,1),ZB(1,1))
      ZL(1,3)=ZSPV(ZLCUR(1,1),ZB(1,1))
      CALL HPMMP(3,4,1,2,1D0,ZA(1,1),ZB(1,1))
      ZR(2,3)=ZSPV(ZRCUR(1,1),ZA(1,1))
      ZL(2,3)=ZSPV(ZLCUR(1,1),ZA(1,1))
      ZR(2,2)=ZSPV(ZRCUR(1,1),ZB(1,1))
      ZL(2,2)=ZSPV(ZLCUR(1,1),ZB(1,1))
C
C HANDLE PMPM AND MPMP
      CALL HPMPM(1,2,3,4,1D0,ZA(1,1),ZB(1,1))
      ZR(1,5)=ZSPV(ZRCUR(1,1),ZA(1,1))
      ZL(1,5)=ZSPV(ZLCUR(1,1),ZA(1,1))
      ZR(1,6)=ZSPV(ZRCUR(1,1),ZB(1,1))
      ZL(1,6)=ZSPV(ZLCUR(1,1),ZB(1,1))
      CALL HPMPM(1,4,3,2,-1D0,ZA(1,1),ZB(1,1))
      ZR(2,5)=ZSPV(ZRCUR(1,1),ZA(1,1))
      ZL(2,5)=ZSPV(ZLCUR(1,1),ZA(1,1))
      ZR(2,6)=ZSPV(ZRCUR(1,1),ZB(1,1))
      ZL(2,6)=ZSPV(ZLCUR(1,1),ZB(1,1))
      CALL HPMPM(3,4,1,2,1D0,ZA(1,1),ZB(1,1))
      ZR(3,5)=ZSPV(ZRCUR(1,1),ZA(1,1))
      ZL(3,5)=ZSPV(ZLCUR(1,1),ZA(1,1))
      ZR(3,6)=ZSPV(ZRCUR(1,1),ZB(1,1))
      ZL(3,6)=ZSPV(ZLCUR(1,1),ZB(1,1))
      CALL HPMPM(3,2,1,4,-1D0,ZA(1,1),ZB(1,1))
      ZR(4,5)=ZSPV(ZRCUR(1,1),ZA(1,1))
      ZL(4,5)=ZSPV(ZLCUR(1,1),ZA(1,1))
      ZR(4,6)=ZSPV(ZRCUR(1,1),ZB(1,1))
      ZL(4,6)=ZSPV(ZLCUR(1,1),ZB(1,1))
C
      END
C
C**********************************************************************
C SUBROUTINE HPMPM CALCULATES THE H-FUNCTION WHEN THE HELICITIES OF
C THE QUARKS ARE +-+-.
C CALL THE H-FUNCTIONS WITH:
C  ARGUMENT 1-4 : QUARK PERMUTATION
C  ARGUMENT 5 IS THE SIGN OF THE H-FUNCTION (EXCHANGE OF TWO
C    IDENTICAL FERMIONS)
C  ARGUMENT 6 IS WHERE TO PUT IT IN ZESSEN
C  ARGUMENT 7 IS WHERE TO PUT ITS COMPLEX CONJUGATE.
C Z1 AND Z2 ARE THE 2*2 COMPLEX MATRICES WHICH ARE THE H-VECTORS
C IN SPINOR LANGUAGE. Z2 IS ALWAYS THE C.C. OF Z1. WHEN THERE
C ARE AN ODD NUMBER OF QUARKS WITH NEGATIVE ENERGY WE HAVE TO ADD
C AN EXTRA MINUS SIGN WHEN COMPLEX CONJUGATING. THIS SIGN IS
C IN THE COMMON /DOTPR/ NEGAEN AND IS CALCULATED BEFORE THROUGH
C THE CALL SETSPN(4,4) IN SQUAR
C**********************************************************************
      SUBROUTINE HPMPM(I1,I2,I3,I4,SIGN,Z1,Z2)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION Z1(2,2),Z2(2,2)
C
      PRO34=2D0*RIN(I3,I4)
      PRO134=PRO34+2D0*(RIN(I1,I3)+RIN(I1,I4))
      PRO234=PRO34+2D0*(RIN(I2,I3)+RIN(I2,I4))
C
      ZF1=-SIGN*ZD(I1,I3)/(PRO34*PRO134)
      ZF2=-SIGN*ZUD(I2,I4)/(PRO34*PRO234)
      DO 10 IX=1,2
        DO 10 IY=1,2
          Z1(IX,IY)=+ZF1*ZKO(I2,IY)*(ZUD(I1,I4)*ZKOD(I1,IX)+
     .                               ZUD(I3,I4)*ZKOD(I3,IX))
     .              -ZF2*ZKOD(I1,IX)*(ZD(I2,I3)*ZKO(I2,IY)+
     .                                ZD(I4,I3)*ZKO(I4,IY))
   10 CONTINUE
      CALL CONGAT(Z1,Z2)
      END
C
C**********************************************************************
C SUBROUTINE HPMMP CALCULATES THE H-FUNCTION WHEN THE HELICITIES OF
C THE QUARKS ARE +--+.  SEE ALSO SUBROUTINE HPMPM
C**********************************************************************
      SUBROUTINE HPMMP(I1,I2,I3,I4,SIGN,Z1,Z2)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION Z1(2,2),Z2(2,2)
C
      PRO34=2D0*RIN(I3,I4)
      PRO134=PRO34+2D0*(RIN(I1,I3)+RIN(I1,I4))
      PRO234=PRO34+2D0*(RIN(I2,I3)+RIN(I2,I4))
C
      ZF1=-SIGN*ZD(I1,I4)/(PRO34*PRO134)
      ZF2=-SIGN*ZUD(I2,I3)/(PRO34*PRO234)
      DO 10 IX=1,2
        DO 10 IY=1,2
          Z1(IX,IY)=+ZF1*ZKO(I2,IY)*(ZUD(I1,I3)*ZKOD(I1,IX)+
     .                               ZUD(I4,I3)*ZKOD(I4,IX))
     .              -ZF2*ZKOD(I1,IX)*(ZD(I2,I4)*ZKO(I2,IY)+
     .                                ZD(I3,I4)*ZKO(I3,IY))
   10 CONTINUE
      CALL CONGAT(Z1,Z2)
      END
C
C**********************************************************************
C MATRIX ELEMENT FOR:  E+ E- --> G,Z --> Q QB Q QB G
C**********************************************************************
C INFORMATION: THE PARTICLES IN PLAB ARE ORDERED AS FOLLOWS:
C             1,2 : INCOMING E+ AND E-
C             3,5 : OUTGOING QUARKS
C             4,6 : OUTGOING ANTI-QUARKS
C             7   : GLUON
C             SW2: SQUARE OF SIN(WEAK ANGLE)
C             RMZ: MASS OF Z
C             RGZ: WIDTH OF Z
C
C   OUTPUT: THE MATRIX ELEMENTS FOR THE SUBPROCESSES RETURNED IN
C   R(3,5): FIRST INDEX  1 = GG
C                        2 = GZ
C                        3 = ZZ
C           SECOND INDEX 1 = SAME QUARKS UP-TYPE
C                        2 = SAME QUARKS DOWN-TYPE
C                        3 = DIFFERENT QUARKS UP-TYPE
C                        4 = DIFFERENT QUARKS DOWN-TYPE
C                        5 = DIFFERENT QUARKS UP - DOWN PAIRS
C**********************************************************************
C   NOT INCLUDED: STRONG COUPLING CONSTANTS!
C                 STATISTICS, SPIN AVERAGES.
C**********************************************************************
C CALLING PROCEDURE FOR SUBROUTINES:
C             1  : ANTI-PARTICLE
C             2  : PARTICLE
C             3,5: LABELS OF QUARKS IN PLAB ARRAY
C             4,6: LABELS OF ANTI-QUARKS IN PLAB ARRAY
C             7  : LABELS THE GLUON
C PARTICLES 1 AND 2 ARE ALWAYS FLIPPED: WE CONSIDER ALL PARTICLE
C TO BE OUTGOING.
C**********************************************************************
      SUBROUTINE EEQ4G(I1,I2,I3,I4,I5,I6,I7,R,SW2,RMZ,RGZ)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      COMMON /MOM/ PLAB(4,10)
      COMMON /KINEMA/ P(0:3,7)
      DIMENSION R(3,5),ISGN(7)
      DATA ISGN/-1,-1,1,1,1,1,1/
C
C TRANSFORM VECTORS
      DO 10 MU=0,3
        NU=MU
        IF(MU.EQ.0) NU=4
        P(MU,6)=ISGN(I1)*PLAB(NU,I1)
        P(MU,7)=ISGN(I2)*PLAB(NU,I2)
        P(MU,1)=ISGN(I3)*PLAB(NU,I3)
        P(MU,2)=ISGN(I4)*PLAB(NU,I4)
        P(MU,3)=ISGN(I5)*PLAB(NU,I5)
        P(MU,4)=ISGN(I6)*PLAB(NU,I6)
        P(MU,5)=ISGN(I7)*PLAB(NU,I7)
   10 CONTINUE
C
C CALCULATE THE INVARIANTS
C
      CALL FILLG
C
C INSERT COUPLINGS TO OBTAIN THE DIFFERENT SUBPROCESSES.
C
      CALL SQUARG(1,1,SW2,RMZ,RGZ,R(1,1))
      CALL SQUARG(2,2,SW2,RMZ,RGZ,R(1,2))
      CALL SQUARG(1,3,SW2,RMZ,RGZ,R(1,3))
      CALL SQUARG(2,4,SW2,RMZ,RGZ,R(1,4))
      CALL SQUARG(1,2,SW2,RMZ,RGZ,R(1,5))
C
      END
C
C**********************************************************************
C FUNCTION SQUARG CALCULATES THE MATRIX ELEMENT SQUARED OF THE
C E+ E- --> G,Z --> Q + QB + Q + QB + GLUON
C**********************************************************************
C DESCIPTION OF THE COMMON ZGQQQQ
C     THE FIRST AND THE SECOND ARE INDICES IN THE SQUARED MATRIX
C     THE LAST INDEX ARE THE DIFFERENT SUBAMPLITUDES (12)
C      1: ++--;+   2:++--;-   3:+--+;+   4:+--+;-
C      8: --++;-   7:--++;+   6:-++-;-   5:-++-;+
C      9: +-+-;+  10:+-+-;-
C     12: -+-+;-  11:-+-+;+
C**********************************************************************
C OUTPUT : R(3)   1: GG   2: GZ  3: ZZ
C**********************************************************************
      SUBROUTINE SQUARG(ITYP1,ITYP2,SW2,RMZ,RGZ,R)
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      PARAMETER(CUP=2D0/3D0,CDOWN=-1D0/3D0)
      PARAMETER(C1=12D0,C2=-8D0/6D0,C3=4D0,C4=0D0,C5=4D0/9D0,
     .          SC2=8D0/6D0,SC3=-4D0)
      COMMON /ZGQQQQ/ ZR(16,12),ZL(16,12)
      DIMENSION S8G(4,2),S8Z(4,2),S16G(2,4),S16Z(2,4),
     .          CHARGE(4),CISOSP(4)
      DIMENSION COL16(16,16),COL8(8,8),R(3)
      DATA CHARGE/CUP,CDOWN,CUP,CDOWN/
      DATA CISOSP/0.5D0,-0.5D0,0.5D0,-0.5D0/
      DATA COL16
     ./ C1, C4, C2, C2, C3, C3,SC3, C4, C4, C1, C2, C2, C3, C3, C4,SC3,
     .  C4, C1, C2, C2, C3, C3, C4,SC3, C1, C4, C2, C2, C3, C3,SC3, C4,
     .  C2, C2,SC2, C4,SC3, C4, C5, C5, C2, C2, C4,SC2, C4,SC3, C5, C5,
     .  C2, C2, C4,SC2, C4,SC3, C5, C5, C2, C2,SC2, C4,SC3, C4, C5, C5,
     .  C3, C3,SC3, C4, C1, C4, C2, C2, C3, C3, C4,SC3, C4, C1, C2, C2,
     .  C3, C3, C4,SC3, C4, C1, C2, C2, C3, C3,SC3, C4, C1, C4, C2, C2,
     . SC3, C4, C5, C5, C2, C2,SC2, C4, C4,SC3, C5, C5, C2, C2, C4,SC2,
     .  C4,SC3, C5, C5, C2, C2, C4,SC2,SC3, C4, C5, C5, C2, C2,SC2, C4,
     .  C4, C1, C2, C2, C3, C3, C4,SC3, C1, C4, C2, C2, C3, C3,SC3, C4,
     .  C1, C4, C2, C2, C3, C3,SC3, C4, C4, C1, C2, C2, C3, C3, C4,SC3,
     .  C2, C2, C4,SC2, C4,SC3, C5, C5, C2, C2,SC2, C4,SC3, C4, C5, C5,
     .  C2, C2,SC2, C4,SC3, C4, C5, C5, C2, C2, C4,SC2, C4,SC3, C5, C5,
     .  C3, C3, C4,SC3, C4, C1, C2, C2, C3, C3,SC3, C4, C1, C4, C2, C2,
     .  C3, C3,SC3, C4, C1, C4, C2, C2, C3, C3, C4,SC3, C4, C1, C2, C2,
     .  C4,SC3, C5, C5, C2, C2, C4,SC2,SC3, C4, C5, C5, C2, C2,SC2, C4,
     . SC3, C4, C5, C5, C2, C2,SC2, C4, C4,SC3, C5, C5, C2, C2, C4,SC2/
      DATA COL8
     ./ C1, C4, C2, C2, C4, C1, C2, C2,
     .  C4, C1, C2, C2, C1, C4, C2, C2,
     .  C2, C2,SC2, C4, C2, C2, C4,SC2,
     .  C2, C2, C4,SC2, C2, C2,SC2, C4,
     .  C4, C1, C2, C2, C1, C4, C2, C2,
     .  C1, C4, C2, C2, C4, C1, C2, C2,
     .  C2, C2, C4,SC2, C2, C2,SC2, C4,
     .  C2, C2,SC2, C4, C2, C2, C4,SC2/
C
C INIT ANSWER
      R(1)=0D0
      R(2)=0D0
      R(3)=0D0
C
C COUPLINGS AND ALL THAT, ENERGY DEPENDENT WIDTH FOR Z-PROPAGATOR.
      CW=DSQRT(1D0-SW2)
      SCW=DSQRT(SW2)*CW
      S=2D0*RINPRO(6,7)
      ZPRG=(0D0,-1D0)/S
      ZPRZ=(0D0,-1D0)/(S-RMZ**2+(0D0,1D0)*S*RGZ/RMZ)
      CGLLEP=1D0
      CGRLEP=1D0
      CG1R=-CHARGE(ITYP1)
      CG2R=-CHARGE(ITYP2)
      CG1L=-CHARGE(ITYP1)
      CG2L=-CHARGE(ITYP2)
      CZLLEP=(-0.5D0+SW2)/SCW
      CZRLEP=SW2/SCW
      CZ1L=(+CISOSP(ITYP1)-SW2*CHARGE(ITYP1))/SCW
      CZ1R=(-SW2*CHARGE(ITYP1))/SCW
      CZ2L=(+CISOSP(ITYP2)-SW2*CHARGE(ITYP2))/SCW
      CZ2R=(-SW2*CHARGE(ITYP2))/SCW
C
      IF(ITYP1.EQ.ITYP2)THEN
        DELTA=1D0
      ELSE
        DELTA=0D0
      END IF
C
C S8(1,.) IS PPMM
      S8G(1,1)=DELTA*CG1R
      S8Z(1,1)=DELTA*CZ1R
      S8G(1,2)=DELTA*CG1L
      S8Z(1,2)=DELTA*CZ1L
C S8(2,.) IS PMMP
      S8G(2,1)=CG1R
      S8Z(2,1)=CZ1R
      S8G(2,2)=CG2L
      S8Z(2,2)=CZ2L
C S8(3,.) IS MPPM
      S8G(3,1)=CG1L
      S8Z(3,1)=CZ1L
      S8G(3,2)=CG2R
      S8Z(3,2)=CZ2R
C S8(4,.) IS MMPP
      S8G(4,1)=DELTA*CG1L
      S8Z(4,1)=DELTA*CZ1L
      S8G(4,2)=DELTA*CG1R
      S8Z(4,2)=DELTA*CZ1R
C
      DO 10 I=1,8
        IJ=(I+1)/2
        DO 11 IX=1,8
          IXA=(IX+3)/4
          DO 12 IY=1,8
            IYA=(IY+3)/4
            R(1)=R(1)+COL8(IX,IY)*S8G(IJ,IXA)*S8G(IJ,IYA)*
     .           ZPRG*CGRLEP*ZR(IX,I)*DCONJG(CGRLEP*ZPRG*ZR(IY,I))
     .               +COL8(IX,IY)*S8G(IJ,IXA)*S8G(IJ,IYA)*
     .           ZPRG*CGLLEP*ZL(IX,I)*DCONJG(ZPRG*CGLLEP*ZL(IY,I))
            R(2)=R(2)+COL8(IX,IY)*S8G(IJ,IXA)*S8Z(IJ,IYA)*
     .           ZPRG*CGRLEP*ZR(IX,I)*DCONJG(ZPRZ*CZRLEP*ZR(IY,I))
     .               +COL8(IX,IY)*S8G(IJ,IXA)*S8Z(IJ,IYA)*
     .           ZPRG*CGLLEP*ZL(IX,I)*DCONJG(ZPRZ*CZLLEP*ZL(IY,I))
     .               +COL8(IX,IY)*S8Z(IJ,IXA)*S8G(IJ,IYA)*
     .           ZPRZ*CZRLEP*ZR(IX,I)*DCONJG(ZPRG*CGRLEP*ZR(IY,I))
     .               +COL8(IX,IY)*S8Z(IJ,IXA)*S8G(IJ,IYA)*
     .           ZPRZ*CZLLEP*ZL(IX,I)*DCONJG(ZPRG*CGLLEP*ZL(IY,I))
            R(3)=R(3)+COL8(IX,IY)*S8Z(IJ,IXA)*S8Z(IJ,IYA)*
     .           ZPRZ*CZRLEP*ZR(IX,I)*DCONJG(ZPRZ*CZRLEP*ZR(IY,I))
     .               +COL8(IX,IY)*S8Z(IJ,IXA)*S8Z(IJ,IYA)*
     .           ZPRZ*CZLLEP*ZL(IX,I)*DCONJG(ZPRZ*CZLLEP*ZL(IY,I))
   12     CONTINUE
   11   CONTINUE
   10 CONTINUE
C
C S16(1,.) IS PMPM
      S16G(1,1)=CG1R
      S16Z(1,1)=CZ1R
      S16G(1,2)=DELTA*CG1R
      S16Z(1,2)=DELTA*CZ1R
      S16G(1,3)=CG2R
      S16Z(1,3)=CZ2R
      S16G(1,4)=DELTA*CG1R
      S16Z(1,4)=DELTA*CZ1R
C S16(2,.) IS MPMP
      S16G(2,1)=CG1L
      S16Z(2,1)=CZ1L
      S16G(2,2)=DELTA*CG1L
      S16Z(2,2)=DELTA*CZ1L
      S16G(2,3)=CG2L
      S16Z(2,3)=CZ2L
      S16G(2,4)=DELTA*CG1L
      S16Z(2,4)=DELTA*CZ1L
C
      DO 20 I=9,12
        IJ=(I-7)/2
        DO 21 IX=1,16
          IXA=(IX+3)/4
          DO 22 IY=1,16
            IYA=(IY+3)/4
            R(1)=R(1)+COL16(IX,IY)*S16G(IJ,IXA)*S16G(IJ,IYA)*
     .           ZPRG*CGRLEP*ZR(IX,I)*DCONJG(CGRLEP*ZPRG*ZR(IY,I))
     .               +COL16(IX,IY)*S16G(IJ,IXA)*S16G(IJ,IYA)*
     .           ZPRG*CGLLEP*ZL(IX,I)*DCONJG(ZPRG*CGLLEP*ZL(IY,I))
            R(2)=R(2)+COL16(IX,IY)*S16G(IJ,IXA)*S16Z(IJ,IYA)*
     .           ZPRG*CGRLEP*ZR(IX,I)*DCONJG(ZPRZ*CZRLEP*ZR(IY,I))
     .               +COL16(IX,IY)*S16G(IJ,IXA)*S16Z(IJ,IYA)*
     .           ZPRG*CGLLEP*ZL(IX,I)*DCONJG(ZPRZ*CZLLEP*ZL(IY,I))
     .               +COL16(IX,IY)*S16Z(IJ,IXA)*S16G(IJ,IYA)*
     .           ZPRZ*CZRLEP*ZR(IX,I)*DCONJG(ZPRG*CGRLEP*ZR(IY,I))
     .               +COL16(IX,IY)*S16Z(IJ,IXA)*S16G(IJ,IYA)*
     .           ZPRZ*CZLLEP*ZL(IX,I)*DCONJG(ZPRG*CGLLEP*ZL(IY,I))
            R(3)=R(3)+COL16(IX,IY)*S16Z(IJ,IXA)*S16Z(IJ,IYA)*
     .           ZPRZ*CZRLEP*ZR(IX,I)*DCONJG(ZPRZ*CZRLEP*ZR(IY,I))
     .               +COL16(IX,IY)*S16Z(IJ,IXA)*S16Z(IJ,IYA)*
     .           ZPRZ*CZLLEP*ZL(IX,I)*DCONJG(ZPRZ*CZLLEP*ZL(IY,I))
   22     CONTINUE
   21   CONTINUE
   20 CONTINUE
C
      END
C
C**********************************************************************
C SUBROUTINE FILLZG CALCULATES THE VARIOUS INVARIANTS NEEDED TO COMPUTE
C 4Q+G CROOS-SECTIONS. THE RESULTS ARE SQUARED WITH THE
C THE COLOR MATRIX AND BROUGHT BACK TO A MINIMAL FORM IN /ZGQQQQ/.
C**********************************************************************
C THE PROPAGATORS ARE NOT INCLUDED TO RESTRICT THE NUMBER OF INVARIANTS
C**********************************************************************
      SUBROUTINE FILLG()
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      COMMON /ZGQQQQ/ ZR(16,12),ZL(16,12)
      DIMENSION ZLCUR(2,2),ZRCUR(2,2)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /ZEVENS/ ZS(2,2,16,12)
      COMMON /LINPR/ RIN(7,7)
C
C THE 5 VECTORS MUST BE IN P(MU,1) .. P(MU,5)
      CALL SETSPN(5,4)
C
      DO 5 I=1,4
        DO 5 J=I+1,5
          RIN(I,J)=RINPRO(I,J)
          RIN(J,I)=RIN(I,J)
    5 CONTINUE
C
      DO 6 IX=1,2
        DO 6 IY=1,2
          ZRCUR(IX,IY)=ZKOD(6,IX)*ZKO(7,IY)
          ZLCUR(IX,IY)=ZKOD(7,IX)*ZKO(6,IY)
    6 CONTINUE
C
C HANDLE ++--;+/- AND --++;+/-
      CALL BADB(1,4,3,2,-1D0,3,1,1,1,8)
      CALL BADB(3,2,1,4,-1D0,4,5,8,5,1)
      CALL BADB(1,4,3,2,-1D0,4,1,2,1,7)
      CALL BADB(3,2,1,4,-1D0,3,5,7,5,2)
C
C HANDLE +--+;+/- AND -++-;+/-
      CALL BADB(1,2,3,4,1D0,3,1,3,1,6)
      CALL BADB(3,4,1,2,1D0,4,5,6,5,3)
      CALL BADB(1,2,3,4,1D0,4,1,4,1,5)
      CALL BADB(3,4,1,2,1D0,3,5,5,5,4)
C
C HANDLE +-+-;+ AND -+-+;-
      CALL BADB(1,2,3,4,1D0,1,1,9,1,12)
      CALL BADB(1,4,3,2,-1D0,1,5,9,5,12)
      CALL BADB(3,4,1,2,1D0,1,9,9,9,12)
      CALL BADB(3,2,1,4,-1D0,1,13,9,13,12)
C
C HANDLE +-+-;- AND -+-+;+
      CALL BADB(1,2,3,4,1D0,2,1,10,1,11)
      CALL BADB(1,4,3,2,-1D0,2,5,10,5,11)
      CALL BADB(3,4,1,2,1D0,2,9,10,9,11)
      CALL BADB(3,2,1,4,-1D0,2,13,10,13,11)
C
      DO 8 I=1,8
        DO 8 J=1,8
          ZR(J,I)=ZSPV(ZRCUR(1,1),ZS(1,1,J,I))
          ZL(J,I)=ZSPV(ZLCUR(1,1),ZS(1,1,J,I))
    8 CONTINUE
C
      DO 9 I=9,12
        DO 9 J=1,16
          ZR(J,I)=ZSPV(ZRCUR(1,1),ZS(1,1,J,I))
          ZL(J,I)=ZSPV(ZLCUR(1,1),ZS(1,1,J,I))
    9 CONTINUE
C
      END
C
C**********************************************************************
C SUBROUTINE BADB CALCULATES VARIOUS SUB-AMPLITUDES ON THE
C THE LEVEL OF BI'S (SEE ARTICLE)
C INDICES 1,2,3 EN 4 DENOTE THE QUARK PERMUTATION.
C INDEX 5 IS THE SIGN OF THE SUBAMPLITUDE.
C THE SUBROUTINE CALCULATES 4 SUB-AMPLITUDES WHICH ARE PUT
C IN THE DATA STRUCTURE /ZEVENS/ ZS(2,2,16,12).
C THEY ARE PUT IN CONSECUTIVE ORDER STARTING AT ZS(.,.,I1A,12A)
C THROUGH ZS(.,.,I1A+3,I2A) FOR THE NORMAL HELICITIES AND
C IN ZS(.,.,I1B,I2B) THROUGH ZS(.,.,I1B+3,I2B) FOR ITS C.C.
C**********************************************************************
C THE HELICITY OF THE FOUR QUARKS AND THE GLUON COMES THROUGH ITYPE.
C THE CONVENTIONS FOR ITYPE ARE: (ZIA SPINOR TENSORS)
C             Q1  Q2   Q3   Q4    G
C   ITYPE=1 ;  +  -    +    -     +
C   ITYPE=2 ;  +  -    +    -     -
C   ITYPE=3 ;  +  -    -    +     +
C   ITYPE=4 ;  +  -    -    +     -
C AND IN ZIB THE COMPLEX CONJUGATED (I.E. FLIPPED) AMPLITUDES.
C**********************************************************************
      SUBROUTINE BADB(I1,I2,I3,I4,SIGN,ITYPE,I1A,I2A,I1B,I2B)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      PARAMETER(TWO32=2.828427124746190D0)
      COMMON /ZEVENS/ ZS(2,2,16,12)
      DIMENSION Z1A(2,2),Z1B(2,2),Z2A(2,2),Z2B(2,2),
     .          Z3A(2,2),Z3B(2,2),Z4A(2,2),Z4B(2,2),
     .          Z5A(2,2),Z5B(2,2),Z6A(2,2),Z6B(2,2),
     .          Z7A(2,2),Z7B(2,2),Z8A(2,2),Z8B(2,2),
     .          Z9A(2,2),Z9B(2,2)
      DIMENSION ZA1(2,2),ZB1(2,2),ZA2(2,2),ZB2(2,2),
     .          ZA3(2,2),ZB3(2,2),ZA4(2,2),ZB4(2,2)
C
      CALL C1ADB(I1,I2,I3,I4,Z1A,Z1B,ITYPE)
      CALL C2ADB(I1,I2,I3,I4,Z2A,Z2B,ITYPE)
      CALL C3ADB(I1,I2,I3,I4,Z3A,Z3B,ITYPE)
      CALL C4ADB(I1,I2,I3,I4,Z4A,Z4B,ITYPE)
      CALL C5ADB(I1,I2,I3,I4,Z5A,Z5B,ITYPE)
      CALL C6ADB(I1,I2,I3,I4,Z6A,Z6B,ITYPE)
      CALL C7ADB(I1,I2,I3,I4,Z7A,Z7B,ITYPE)
      CALL C8ADB(I1,I2,I3,I4,Z8A,Z8B,ITYPE)
      CALL C9ADB(I1,I2,I3,I4,Z9A,Z9B,ITYPE)
C
      DO 10 IX=1,2
       DO 10 IY=1,2
        ZA1(IX,IY)=0.5D0*(Z1A(IX,IY)+Z2A(IX,IY)+Z3A(IX,IY)+Z4A(IX,IY))
        ZB1(IX,IY)=0.5D0*(Z1B(IX,IY)+Z2B(IX,IY)+Z3B(IX,IY)+Z4B(IX,IY))
        ZA2(IX,IY)=0.5D0*(-Z3A(IX,IY)-Z4A(IX,IY)+Z5A(IX,IY)+Z6A(IX,IY))
        ZB2(IX,IY)=0.5D0*(-Z3B(IX,IY)-Z4B(IX,IY)+Z5B(IX,IY)+Z6B(IX,IY))
        ZA3(IX,IY)=0.5D0*(Z7A(IX,IY))
        ZB3(IX,IY)=0.5D0*(Z7B(IX,IY))
        ZA4(IX,IY)=0.5D0*(Z8A(IX,IY)+Z9A(IX,IY))
        ZB4(IX,IY)=0.5D0*(Z8B(IX,IY)+Z9B(IX,IY))
   10 CONTINUE
C
      DO 20 IX=1,2
        DO 20 IY=1,2
          ZS(IX,IY,I1A,I2A)=SIGN*TWO32*ZA1(IX,IY)
          ZS(IX,IY,I1B,I2B)=SIGN*TWO32*ZB1(IX,IY)
          ZS(IX,IY,I1A+1,I2A)=SIGN*TWO32*ZA2(IX,IY)
          ZS(IX,IY,I1B+1,I2B)=SIGN*TWO32*ZB2(IX,IY)
          ZS(IX,IY,I1A+2,I2A)=SIGN*TWO32*ZA3(IX,IY)
          ZS(IX,IY,I1B+2,I2B)=SIGN*TWO32*ZB3(IX,IY)
          ZS(IX,IY,I1A+3,I2A)=SIGN*TWO32*ZA4(IX,IY)
          ZS(IX,IY,I1B+3,I2B)=SIGN*TWO32*ZB4(IX,IY)
   20 CONTINUE
C
      END
C
C**********************************************************************
C SUBROUTINE C1ADB THROUGH C9ADB CALCULATE VARIOUS SUB-AMPLITUDES
C INDICES 1,2,3 EN 4 ARE THE QUARK PERMUTATION.
C INDEX 5 IS THE SPINOR-TENSOR THAT IS GOING TO CONTAIN THE
C AMPLITUDE. INDEX 6 IS ITS COMPLEX CONJUGATION. TAKE CARE OF
C THE EXTRA MINUS SIGN THROUGH NEGAEN (SEE SUBROUTINE CONGAT).
C**********************************************************************
C THE HELICITY OF THE FOUR QUARKS AND THE GLUON COMES THROUGH ITYPE.
C THE CONVENTIONS FOR ITYPE ARE:
C             Q1  Q2   Q3   Q4    G
C   ITYPE=1 ;  +  -    +    -     +
C   ITYPE=2 ;  +  -    +    -     -
C   ITYPE=3 ;  +  -    -    +     +
C   ITYPE=4 ;  +  -    -    +     -
C AND IN Z2 THE COMPLEX CONJUGATED (I.E. FLIPPED) AMPLITUDES.
C**********************************************************************
      SUBROUTINE C1ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION Z1(2,2),Z2(2,2),ZH1(2),ZH2(2)
C
      PRO34=2D0*RIN(I3,I4)
      PRO34K=PRO34+2D0*(RIN(5,I3)+RIN(5,I4))
      PR234K=PRO34K+2D0*(RIN(I2,I3)+RIN(I2,I4)+RIN(I2,5))
      DO 5 I=1,2
        ZH1(I)=ZD(I2,I3)*ZKO(I2,I)+ZD(I4,I3)*ZKO(I4,I)+ZD(5,I3)*ZKO(5,I)
        ZH2(I)=ZD(I2,5)*ZKO(I2,I)+ZD(I3,5)*ZKO(I3,I)+ZD(I4,5)*ZKO(I4,I)
    5 CONTINUE
      IF (ITYPE.EQ.1) THEN
        ZF1=-ZUD(I2,I4)/(ZUD(I3,5)*ZUD(5,I2)*PRO34K*PR234K)
        ZF2=ZUD(I2,I4)*(ZD(5,I3)*ZUD(I2,I3)
     .       +ZD(5,I4)*ZUD(I2,I4))/(ZUD(5,I2)*PRO34*PRO34K*PR234K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*(ZH1(IY)*ZUD(I2,I3)+
     .                                  ZH2(IY)*ZUD(I2,5))
     .                +ZF2*ZKOD(I1,IX)*ZH1(IY)
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=-(ZD(I3,I2)*ZUD(I4,I2)+ZD(I3,5)*ZUD(I4,5))/
     .         (ZD(I3,5)*ZD(5,I2)*PRO34*PR234K)
        ZF2=-ZUD(5,I4)*ZUD(I2,I4)/
     .         (ZD(I3,5)*ZUD(I3,I4)*PRO34K*PR234K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*ZH1(IY)
     .                +ZF2*ZKOD(I1,IX)*ZH1(IY)
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=-ZUD(I2,I3)**2/(ZUD(I3,5)*ZUD(5,I2)*PRO34*PR234K)
     .      +ZD(I4,5)*ZUD(I2,I3)/(ZUD(I3,5)*ZD(I3,I4)*PRO34K*PR234K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .       (+ZD(I2,I4)*ZKO(I2,IY)+ZD(I3,I4)*ZKO(I3,IY)
     .        +ZD(5,I4)*ZKO(5,IY))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=-(ZUD(I2,I3)*ZD(I2,I3)+ZUD(5,I3)*ZD(5,I3))/
     .         (ZD(I2,5)*ZD(5,I3)*PRO34*PR234K)
     .      +ZUD(I2,I4)*ZUD(I3,5)/(ZD(I3,5)*ZUD(I3,I4)*PRO34K*PR234K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .       (+ZD(I2,I4)*ZKO(I2,IY)+ZD(I3,I4)*ZKO(I3,IY)
     .        +ZD(5,I4)*ZKO(5,IY))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
      END
C
      SUBROUTINE C2ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION Z1(2,2),Z2(2,2)
C
      PRO34=2D0*RIN(I3,I4)
      PRO34K=PRO34+2D0*(RIN(5,I3)+RIN(5,I4))
      PRO134=PRO34+2D0*(RIN(I1,I3)+RIN(I1,I4))
      PR134K=PRO34K+2D0*(RIN(I1,5)+RIN(I1,I4)+RIN(I1,I3))
      IF (ITYPE.EQ.1) THEN
        ZF1=ZD(I1,I3)*(ZD(5,I1)*ZUD(I4,I1)+
     .         ZD(5,I3)*ZUD(I4,I3))/(ZUD(I2,5)*PRO34*PRO134*PR134K)
        ZF2=(ZD(I1,I3)*ZUD(I2,I3)+ZD(I1,5)*ZUD(I2,5))/
     .         (ZUD(I3,5)*ZUD(5,I2)*PRO34K*PR134K)
     .     +ZD(I1,I3)*(ZD(5,I3)*ZUD(I2,I3)
     .         +ZD(5,I4)*ZUD(I2,I4))/(ZUD(I2,5)*PRO34*PRO34K*PR134K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .       (ZUD(I1,I2)*ZKOD(I1,IX)+ZUD(I3,I2)*ZKOD(I3,IX)+
     .        ZUD(I4,I2)*ZKOD(I4,IX)+ZUD(5,I2)*ZKOD(5,IX))
     .                +ZF2*ZKO(I2,IY)*
     .       (ZUD(I1,I4)*ZKOD(I1,IX)+ZUD(I3,I4)*ZKOD(I3,IX)
     .        +ZUD(5,I4)*ZKOD(5,IX))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=ZD(I1,I3)/(ZD(I3,5)*ZD(5,I2)*PRO34*PRO134)
        ZF2=ZD(I1,I3)**2*ZUD(I1,I4)/(ZD(I3,5)*PRO34*PRO134*PR134K)
        ZF3=ZUD(5,I4)*ZD(I1,I3)/(ZD(I3,5)*ZUD(I3,I4)*PRO34K*PR134K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*
     .          (ZUD(I1,I4)*ZKOD(I1,IX)+ZUD(I3,I4)*ZKOD(I3,IX))*
     .          (ZD(I2,I3)*ZKO(I2,IY)+ZD(5,I3)*ZKO(5,IY))
     .                +ZF2*ZKO(I2,IY)*
     .          (+ZUD(I1,5)*ZKOD(I1,IX)
     .           +ZUD(I3,5)*ZKOD(I3,IX)
     .           +ZUD(I4,5)*ZKOD(I4,IX))
     .                +ZF3*ZKO(I2,IY)*
     .          (+ZUD(I1,I4)*ZKOD(I1,IX)
     .           +ZUD(I3,I4)*ZKOD(I3,IX)
     .           +ZUD(5,I4)*ZKOD(5,IX))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=ZUD(I2,I3)*ZD(I1,I4)/(ZUD(I3,5)*ZUD(5,I2)*PRO34*PRO134)
        ZF2=ZD(I1,I4)*(ZD(5,I1)*ZUD(I3,I1)+
     .      ZD(5,I4)*ZUD(I3,I4))/(ZUD(I3,5)*PRO34*PRO134*PR134K)
     .     -ZD(I1,I4)*ZD(I4,5)/(ZUD(I3,5)*ZD(I3,I4)*PRO34K*PR134K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .          (ZUD(I1,I3)*ZKOD(I1,IX)+ZUD(I4,I3)*ZKOD(I4,IX))
     .                +ZF2*ZKO(I2,IY)*
     .          (+ZUD(I1,I3)*ZKOD(I1,IX)
     .           +ZUD(I4,I3)*ZKOD(I4,IX)
     .           +ZUD(5,I3)*ZKOD(5,IX))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=ZD(I1,I4)/(ZD(I2,5)*ZD(5,I3)*PRO34*PRO134)
        ZF2=ZD(I1,I4)*(ZD(I3,I1)*ZUD(I3,I1)+
     .      ZD(I3,I4)*ZUD(I3,I4))/(ZD(I3,5)*PRO34*PRO134*PR134K)
     .     -ZD(I1,I4)/(ZD(I3,5)*PRO34K*PR134K)
        ZF3=-ZD(I1,I4)*ZUD(I4,5)/(ZD(I3,5)*ZUD(I3,I4)*PRO34K*PR134K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*
     .          (ZUD(I1,I3)*ZKOD(I1,IX)+ZUD(I4,I3)*ZKOD(I4,IX))*
     .          (ZD(I2,I3)*ZKO(I2,IY)+ZD(5,I3)*ZKO(5,IY))
     .                +ZF2*ZKO(I2,IY)*
     .          (+ZUD(I1,5)*ZKOD(I1,IX)
     .           +ZUD(I3,5)*ZKOD(I3,IX)
     .           +ZUD(I4,5)*ZKOD(I4,IX))
     .                +ZF3*ZKO(I2,IY)*
     .          (+ZUD(I1,I3)*ZKOD(I1,IX)+ZUD(I4,I3)*ZKOD(I4,IX)
     .           +ZUD(5,I3)*ZKOD(5,IX))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
      END
C
      SUBROUTINE C3ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION Z1(2,2),Z2(2,2)
C
      PRO34=2D0*RIN(I3,I4)
      PRO34K=PRO34+2D0*(RIN(5,I3)+RIN(5,I4))
      PR234K=PRO34K+2D0*(RIN(I2,I3)+RIN(I2,I4)+RIN(I2,5))
      IF (ITYPE.EQ.1) THEN
        ZF1=ZUD(I2,I4)*ZD(I3,5)/(PRO34*PRO34K*PR234K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .         (+ZD(I2,5)*ZKO(I2,IY)
     .          +ZD(I3,5)*ZKO(I3,IY)
     .          +ZD(I4,5)*ZKO(I4,IY))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=ZUD(I2,5)*ZUD(I4,5)/(PRO34*PRO34K*PR234K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .         (+ZD(I2,I3)*ZKO(I2,IY)
     .          +ZD(I4,I3)*ZKO(I4,IY)
     .          +ZD(5,I3)*ZKO(5,IY))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=ZUD(I2,I3)*ZD(I4,5)/(PRO34*PRO34K*PR234K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .         (+ZD(I2,5)*ZKO(I2,IY)
     .          +ZD(I3,5)*ZKO(I3,IY)
     .          +ZD(I4,5)*ZKO(I4,IY))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=ZUD(I2,5)*ZUD(I3,5)/(PRO34*PRO34K*PR234K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .         (+ZD(I2,I4)*ZKO(I2,IY)
     .          +ZD(I3,I4)*ZKO(I3,IY)
     .          +ZD(5,I4)*ZKO(5,IY))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
      END
C
      SUBROUTINE C4ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION Z1(2,2),Z2(2,2)
C
      PRO34=2D0*RIN(I3,I4)
      PRO34K=PRO34+2D0*(RIN(5,I3)+RIN(5,I4))
      PR134K=PRO34K+2D0*(RIN(I1,5)+RIN(I1,I4)+RIN(I1,I3))
      IF (ITYPE.EQ.1) THEN
        ZF1=-ZD(I1,5)*ZD(I3,5)/(PRO34*PRO34K*PR134K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .       (+ZUD(I1,I4)*ZKOD(I1,IX)
     .        +ZUD(I3,I4)*ZKOD(I3,IX)
     .        +ZUD(5,I4)*ZKOD(5,IX))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=-ZUD(I4,5)*ZD(I1,I3)/(PRO34*PRO34K*PR134K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .       (+ZUD(I1,5)*ZKOD(I1,IX)
     .        +ZUD(I3,5)*ZKOD(I3,IX)
     .        +ZUD(I4,5)*ZKOD(I4,IX))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=-ZD(I1,5)*ZD(I4,5)/(PRO34*PRO34K*PR134K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .       (+ZUD(I1,I3)*ZKOD(I1,IX)
     .        +ZUD(I4,I3)*ZKOD(I4,IX)
     .        +ZUD(5,I3)*ZKOD(5,IX))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=-ZD(I1,I4)*ZUD(I3,5)/(PRO34*PRO34K*PR134K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .       (+ZUD(I1,5)*ZKOD(I1,IX)
     .        +ZUD(I3,5)*ZKOD(I3,IX)
     .        +ZUD(I4,5)*ZKOD(I4,IX))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
      END
C
      SUBROUTINE C5ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION Z1(2,2),Z2(2,2)
C
      PRO34=2D0*RIN(I3,I4)
      PRO34K=PRO34+2D0*(RIN(5,I3)+RIN(5,I4))
      PRO234=PRO34+2D0*(RIN(I2,I3)+RIN(I2,I4))
      PR234K=PRO34K+2D0*(RIN(I2,I3)+RIN(I2,I4)+RIN(I2,5))
      IF (ITYPE.EQ.1) THEN
        ZF1=ZUD(I2,I4)**2*ZD(I2,I3)/(ZUD(I4,5)*PRO34*PRO234*PR234K)
        ZF2=ZUD(I2,I4)/(ZUD(I1,5)*ZUD(5,I4)*PRO34*PRO234)
        ZF3=ZUD(I2,I4)*ZD(I3,5)/(ZD(I3,I4)*ZUD(I4,5)*PRO34K*PR234K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .        (+ZD(I2,5)*ZKO(I2,IY)+ZD(I3,5)*ZKO(I3,IY)
     .         +ZD(I4,5)*ZKO(I4,IY))
     .                +ZF2*
     .        (ZUD(I1,I4)*ZKOD(I1,IX)+ZUD(5,I4)*ZKOD(5,IX))*
     .        (ZD(I2,I3)*ZKO(I2,IY)+ZD(I4,I3)*ZKO(I4,IY))
     .                +ZF3*ZKOD(I1,IX)*
     .        (+ZD(I2,I3)*ZKO(I2,IY)+ZD(I4,I3)*ZKO(I4,IY)
     .         +ZD(5,I3)*ZKO(5,IY))
   10   CONTINUE
      END IF
C
      IF (ITYPE.EQ.2) THEN
        ZF1=ZUD(I2,I4)*(ZD(I3,I2)*ZUD(5,I2)+
     .      ZD(I3,I4)*ZUD(5,I4))/(ZD(I1,5)*PRO34*PRO234*PR234K)
        ZF2=(ZD(I1,I4)*ZUD(I2,I4)+ZD(I1,5)*ZUD(I2,5))/
     .      (ZD(I1,5)*ZD(5,I4)*PRO34K*PR234K)
     .     +ZUD(I2,I4)*(ZD(I1,I3)*ZUD(5,I3)+
     .      ZD(I1,I4)*ZUD(5,I4))/(ZD(I1,5)*PRO34*PRO34K*PR234K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .         (+ZD(I2,I1)*ZKO(I2,IY)
     .          +ZD(I3,I1)*ZKO(I3,IY)
     .          +ZD(I4,I1)*ZKO(I4,IY)
     .          +ZD(5,I1)*ZKO(5,IY))
     .                +ZF2*ZKOD(I1,IX)*
     .         (+ZD(I2,I3)*ZKO(I2,IY)
     .          +ZD(I4,I3)*ZKO(I4,IY)
     .          +ZD(5,I3)*ZKO(5,IY))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=+ZUD(I2,I3)*(PRO34+ZD(I4,I2)*ZUD(I4,I2))/
     .      (ZUD(I4,5)*PRO34*PRO234*PR234K)
     .      -ZUD(I2,I3)/(ZUD(I4,5)*PRO34K*PR234K)
        ZF2=+ZUD(I2,I3)/(ZUD(I1,5)*ZUD(5,I4)*PRO34*PRO234)
        ZF3=-ZUD(I2,I3)*ZD(5,I3)/(ZUD(I4,5)*ZD(I3,I4)*PRO34K*PR234K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .         (+ZD(I2,5)*ZKO(I2,IY)
     .          +ZD(I3,5)*ZKO(I3,IY)
     .          +ZD(I4,5)*ZKO(I4,IY))
     .                +ZF2*
     .         (ZUD(I1,I4)*ZKOD(I1,IX)+ZUD(5,I4)*ZKOD(5,IX))*
     .         (ZD(I2,I4)*ZKO(I2,IY)+ZD(I3,I4)*ZKO(I3,IY))
     .                +ZF3*ZKOD(I1,IX)*
     .         (+ZD(I2,I4)*ZKO(I2,IY)
     .          +ZD(I3,I4)*ZKO(I3,IY)
     .          +ZD(5,I4)*ZKO(5,IY))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=+ZUD(I2,I3)*(ZD(I4,I2)*ZUD(5,I2)+
     .      ZD(I4,I3)*ZUD(5,I3))/(ZD(I4,5)*PRO34*PRO234*PR234K)
     .      +ZUD(I2,I3)*ZUD(I3,5)/(ZD(I4,5)*ZUD(I3,I4)*PRO34K*PR234K)
        ZF2=+ZUD(I2,I3)*ZD(I1,I4)/(ZD(I1,5)*ZD(5,I4)*PRO34*PRO234)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .         (+ZD(I2,I4)*ZKO(I2,IY)
     .          +ZD(I3,I4)*ZKO(I3,IY)
     .          +ZD(5,I4)*ZKO(5,IY))
     .                +ZF2*ZKOD(I1,IX)*
     .         (ZD(I2,I4)*ZKO(I2,IY)+ZD(I3,I4)*ZKO(I3,IY))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
      END
C
      SUBROUTINE C6ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION Z1(2,2),Z2(2,2)
C
      PRO34=2D0*RIN(I3,I4)
      PRO34K=PRO34+2D0*(RIN(5,I3)+RIN(5,I4))
      PR134K=PRO34K+2D0*(RIN(I1,I3)+RIN(I1,I4)+RIN(I1,5))
      IF (ITYPE.EQ.1) THEN
        ZF1=(ZD(I3,I1)*ZUD(I4,I1)+ZD(I3,5)*ZUD(I4,5))/
     .      (ZUD(I4,5)*ZUD(I1,5)*PRO34*PR134K)
     .     -ZD(I1,I3)*ZD(I3,5)/(ZUD(I4,5)*ZD(I3,I4)*PRO34K*PR134K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .        (+ZUD(I1,I4)*ZKOD(I1,IX)
     .         +ZUD(I3,I4)*ZKOD(I3,IX)
     .         +ZUD(5,I4)*ZKOD(5,IX))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=-ZD(I1,I3)/(ZD(I1,5)*ZD(5,I4)*PRO34K*PR134K)
        ZF2=-ZD(I1,I3)*(ZD(I1,I3)*ZUD(5,I3)+
     .        ZD(I1,I4)*ZUD(5,I4))/(ZD(I1,5)*PRO34*PRO34K*PR134K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .        (+(ZUD(I1,I4)*ZD(I1,I4)+ZUD(I1,5)*ZD(I1,5))*ZKOD(I1,IX)
     .         +(ZUD(I3,I4)*ZD(I1,I4)+ZUD(I3,5)*ZD(I1,5))*ZKOD(I3,IX)
     .         +(ZUD(I4,I4)*ZD(I1,I4)+ZUD(I4,5)*ZD(I1,5))*ZKOD(I4,IX)
     .         +(ZUD(5,I4)*ZD(I1,I4)+ZUD(5,5)*ZD(I1,5))*ZKOD(5,IX))
     .                +ZF2*ZKO(I2,IY)*
     .         (+ZUD(I1,I4)*ZKOD(I1,IX)
     .          +ZUD(I3,I4)*ZKOD(I3,IX)
     .          +ZUD(5,I4)*ZKOD(5,IX))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=-(ZD(I1,I4)*ZUD(I1,I4)+ZD(5,I4)*ZUD(5,I4))/
     .      (ZUD(I1,5)*ZUD(5,I4)*PRO34*PR134K)
     .      +ZD(I1,5)/(ZUD(I4,5)*PRO34K*PR134K)
     .      -ZD(I1,I4)*ZD(I3,5)/(ZUD(I4,5)*ZD(I3,I4)*PRO34K*PR134K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .         (+ZUD(I1,I3)*ZKOD(I1,IX)
     .          +ZUD(I4,I3)*ZKOD(I4,IX)
     .          +ZUD(5,I3)*ZKOD(5,IX))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=-ZD(I1,I4)**2/(ZD(I1,5)*ZD(5,I4)*PRO34*PR134K)
     .      -ZD(I1,I4)*ZUD(I3,5)/(ZD(I4,5)*ZUD(I3,I4)*PRO34K*PR134K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .         (+ZUD(I1,I3)*ZKOD(I1,IX)
     .          +ZUD(I4,I3)*ZKOD(I4,IX)
     .          +ZUD(5,I3)*ZKOD(5,IX))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
      END
C
      SUBROUTINE C7ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION Z1(2,2),Z2(2,2)
C
      PRO34=2D0*RIN(I3,I4)
      PRO34K=PRO34+2D0*(RIN(5,I3)+RIN(5,I4))
      PR134K=PRO34K+2D0*(RIN(I1,I3)+RIN(I1,I4)+RIN(I1,5))
      PR234K=PRO34K+2D0*(RIN(I2,I3)+RIN(I2,I4)+RIN(I2,5))
      IF (ITYPE.EQ.1) THEN
        ZF1=(ZD(I1,I3)*ZUD(I4,I3)+ZD(I1,5)*ZUD(I4,5))/
     .      (ZUD(I3,5)*ZUD(5,I4)*PRO34K*PR134K)
        ZF2=-ZUD(I2,I4)/(ZUD(I3,5)*ZUD(5,I4)*PRO34K*PR234K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .        (+ZUD(I1,I4)*ZKOD(I1,IX)
     .         +ZUD(I3,I4)*ZKOD(I3,IX)
     .         +ZUD(5,I4)*ZKOD(5,IX))
     .                +ZF2*ZKOD(I1,IX)*
     .        (+ZKO(I2,IY)*(ZD(I2,I3)*ZUD(I4,I3)+ZD(I2,5)*ZUD(I4,5))
     .         +ZKO(I3,IY)*(ZD(I3,I3)*ZUD(I4,I3)+ZD(I3,5)*ZUD(I4,5))
     .         +ZKO(I4,IY)*(ZD(I4,I3)*ZUD(I4,I3)+ZD(I4,5)*ZUD(I4,5))
     .         +ZKO(5,IY)*(ZD(5,I3)*ZUD(I4,I3)+ZD(5,5)*ZUD(I4,5)))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=(ZD(I3,I4)*ZUD(I2,I4)+ZD(I3,5)*ZUD(I2,5))/
     .      (ZD(I3,5)*ZD(5,I4)*PRO34K*PR234K)
        ZF2=-ZD(I1,I3)/(ZD(I3,5)*ZD(5,I4)*PRO34K*PR134K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .        (+ZD(I2,I3)*ZKO(I2,IY)+ZD(I4,I3)*ZKO(I4,IY)
     .         +ZD(5,I3)*ZKO(5,IY))
     .                +ZF2*ZKO(I2,IY)*
     .        (+(ZUD(I1,I4)*ZD(I3,I4)+ZUD(I1,5)*ZD(I3,5))*ZKOD(I1,IX)
     .         +(ZUD(I3,I4)*ZD(I3,I4)+ZUD(I3,5)*ZD(I3,5))*ZKOD(I3,IX)
     .         +(ZUD(I4,I4)*ZD(I3,I4)+ZUD(I4,5)*ZD(I3,5))*ZKOD(I4,IX)
     .         +(ZUD(5,I4)*ZD(I3,I4)+ZUD(5,5)*ZD(I3,5))*ZKOD(5,IX))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=ZUD(I2,I3)/(ZUD(I3,5)*ZUD(5,I4)*PRO34K*PR234K)
        ZF2=-(ZD(I1,I4)*ZUD(I3,I4)+ZD(I1,5)*ZUD(I3,5))/
     .      (ZUD(I3,5)*ZUD(5,I4)*PRO34K*PR134K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .        (+(ZUD(I3,I4)*ZD(I2,I4)+ZUD(I3,5)*ZD(I2,5))*ZKO(I2,IY)
     .         +(ZUD(I3,I4)*ZD(I3,I4)+ZUD(I3,5)*ZD(I3,5))*ZKO(I3,IY)
     .         +(ZUD(I3,I4)*ZD(I4,I4)+ZUD(I3,5)*ZD(I4,5))*ZKO(I4,IY)
     .         +(ZUD(I3,I4)*ZD(5,I4)+ZUD(I3,5)*ZD(5,5))*ZKO(5,IY))
     .                +ZF2*ZKO(I2,IY)*
     .        (ZUD(I1,I3)*ZKOD(I1,IX)+ZUD(I4,I3)*ZKOD(I4,IX)+
     .         ZUD(5,I3)*ZKOD(5,IX))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=-(ZD(I4,I3)*ZUD(I2,I3)+ZD(I4,5)*ZUD(I2,5))/
     .      (ZD(I3,5)*ZD(5,I4)*PRO34K*PR234K)
        ZF2=+ZD(I1,I4)/(ZD(I3,5)*ZD(5,I4)*PRO34K*PR134K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .        (ZD(I2,I4)*ZKO(I2,IY)+ZD(I3,I4)*ZKO(I3,IY)
     .         +ZD(5,I4)*ZKO(5,IY))
     .                +ZF2*ZKO(I2,IY)*
     .        (+ZKOD(I1,IX)*(ZUD(I1,I3)*ZD(I4,I3)+ZUD(I1,5)*ZD(I4,5))
     .         +ZKOD(I3,IX)*(ZUD(I3,I3)*ZD(I4,I3)+ZUD(I3,5)*ZD(I4,5))
     .         +ZKOD(I4,IX)*(ZUD(I4,I3)*ZD(I4,I3)+ZUD(I4,5)*ZD(I4,5))
     .         +ZKOD(5,IX) *(ZUD(5,I3) *ZD(I4,I3)+ZUD(5,5)*ZD(I4,5)))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
      END
C
      SUBROUTINE C8ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION Z1(2,2),Z2(2,2)
C
      PRO34=2D0*RIN(I3,I4)
      PRO234=PRO34+2D0*(RIN(I2,I3)+RIN(I2,I4))
      PR234K=PRO234+2D0*(RIN(5,I2)+RIN(5,I3)+RIN(5,I4))
      IF (ITYPE.EQ.1) THEN
        ZF1=ZUD(I2,I4)/(ZUD(I1,5)*ZUD(5,I2)*PRO34*PRO234)
        ZF2=ZUD(I2,I4)**2/
     .      (ZUD(I2,5)*ZUD(I3,I4)*PRO234*PR234K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*
     .        (+ZKOD(I1,IX)*ZUD(I1,I2)+ZKOD(5,IX)*ZUD(5,I2))*
     .        (+ZKO(I2,IY)*ZD(I2,I3)+ZKO(I4,IY)*ZD(I4,I3))
     .                +ZF2*ZKOD(I1,IX)*
     .        (+ZKO(I2,IY)*ZD(I2,5)+ZKO(I3,IY)*ZD(I3,5)
     .         +ZKO(I4,IY)*ZD(I4,5))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=ZUD(I2,I4)*(ZD(I3,I2)*ZUD(5,I2)+
     .      ZD(I3,I4)*ZUD(5,I4))/(ZD(I1,5)*PRO34*PRO234*PR234K)
        ZF2=-(ZD(I1,I2)*ZUD(I4,I2)+ZD(I1,5)*ZUD(I4,5))/
     .      (ZD(I1,5)*ZD(5,I2)*PRO34*PR234K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .        (+ZD(I2,I1)*ZKO(I2,IY)+ZD(I3,I1)*ZKO(I3,IY)
     .         +ZD(I4,I1)*ZKO(I4,IY)+ZD(5,I1)*ZKO(5,IY))
     .                +ZF2*ZKOD(I1,IX)*
     .        (+ZD(I2,I3)*ZKO(I2,IY)+ZD(I4,I3)*ZKO(I4,IY)
     .         +ZD(5,I3)*ZKO(5,IY))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=ZUD(I2,I3)/(ZUD(I1,5)*ZUD(5,I2)*PRO34*PRO234)
        ZF2=-ZUD(I2,I3)**2/
     .      (ZUD(I2,5)*ZUD(I3,I4)*PRO234*PR234K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*
     .        (+ZUD(I1,I2)*ZKOD(I1,IX)+ZUD(5,I2)*ZKOD(5,IX))*
     .        (+ZD(I2,I4)*ZKO(I2,IY)+ZD(I3,I4)*ZKO(I3,IY))
     .                +ZF2*ZKOD(I1,IX)*
     .        (+ZD(I2,5)*ZKO(I2,IY)+ZD(I3,5)*ZKO(I3,IY)
     .         +ZD(I4,5)*ZKO(I4,IY))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=ZUD(I2,I3)*(ZD(I4,I2)*ZUD(5,I2)+
     .      ZD(I4,I3)*ZUD(5,I3))/(ZD(I1,5)*PRO34*PRO234*PR234K)
        ZF2=-(ZD(I1,I2)*ZUD(I3,I2)+ZD(I1,5)*ZUD(I3,5))/
     .      (ZD(I1,5)*ZD(5,I2)*PRO34*PR234K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*ZKOD(I1,IX)*
     .        (+ZD(I2,I1)*ZKO(I2,IY)+ZD(I3,I1)*ZKO(I3,IY)
     .         +ZD(I4,I1)*ZKO(I4,IY)+ZD(5,I1)*ZKO(5,IY))
     .                +ZF2*ZKOD(I1,IX)*
     .        (+ZD(I2,I4)*ZKO(I2,IY)+ZD(I3,I4)*ZKO(I3,IY)
     .         +ZD(5,I4)*ZKO(5,IY))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
      END
C
      SUBROUTINE C9ADB(I1,I2,I3,I4,Z1,Z2,ITYPE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      COMMON /LINPR/ RIN(7,7)
      DIMENSION Z1(2,2),Z2(2,2)
C
      PRO34=2D0*RIN(I3,I4)
      PRO134=PRO34+2D0*(RIN(I1,I3)+RIN(I1,I4))
      PR134K=PRO134+2D0*(RIN(5,I1)+RIN(5,I3)+RIN(5,I4))
      IF (ITYPE.EQ.1) THEN
        ZF1=-(ZD(I3,I1)*ZUD(I2,I1)+ZD(I3,5)*ZUD(I2,5))/
     .      (ZUD(I1,5)*ZUD(5,I2)*PRO34*PR134K)
        ZF2=ZD(I1,I3)*(ZD(5,I1)*ZUD(I4,I1)
     .      +ZD(5,I3)*ZUD(I4,I3))/(ZUD(I2,5)*PRO34*PRO134*PR134K)
        DO 10 IX=1,2
          DO 10 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .        (+ZKOD(I1,IX)*ZUD(I1,I4)+ZKOD(I3,IX)*ZUD(I3,I4)
     .         +ZKOD(5,IX)*ZUD(5,I4))
     .                +ZF2*ZKO(I2,IY)*
     .        (+ZKOD(I1,IX)*ZUD(I1,I2)+ZKOD(I3,IX)*ZUD(I3,I2)
     .         +ZKOD(I4,IX)*ZUD(I4,I2)+ZKOD(5,IX)*ZUD(5,I2))
   10   CONTINUE
      END IF
      IF (ITYPE.EQ.2) THEN
        ZF1=ZD(I1,I3)/(ZD(I1,5)*ZD(5,I2)*PRO34*PRO134)
        ZF2=-ZD(I1,I3)**2/(ZD(I1,5)*ZD(I3,I4)*PRO134*PR134K)
        DO 20 IX=1,2
          DO 20 IY=1,2
            Z1(IX,IY)=+ZF1*
     .        (+ZUD(I1,I4)*ZKOD(I1,IX)+ZUD(I3,I4)*ZKOD(I3,IX))*
     .        (+ZD(I2,I1)*ZKO(I2,IY)+ZD(5,I1)*ZKO(5,IY))
     .                +ZF2*ZKO(I2,IY)*
     .        (+ZUD(I1,5)*ZKOD(I1,IX)+ZUD(I3,5)*ZKOD(I3,IX)
     .         +ZUD(I4,5)*ZKOD(I4,IX))
   20   CONTINUE
      END IF
      IF (ITYPE.EQ.3) THEN
        ZF1=-(ZD(I4,I1)*ZUD(I2,I1)+ZD(I4,5)*ZUD(I2,5))/
     .      (ZUD(I1,5)*ZUD(5,I2)*PRO34*PR134K)
        ZF2=ZD(I1,I4)*(ZD(5,I1)*ZUD(I3,I1)+
     .      ZD(5,I4)*ZUD(I3,I4))/(ZUD(I2,5)*PRO34*PRO134*PR134K)
        DO 30 IX=1,2
          DO 30 IY=1,2
            Z1(IX,IY)=+ZF1*ZKO(I2,IY)*
     .        (ZUD(I1,I3)*ZKOD(I1,IX)+ZUD(I4,I3)*ZKOD(I4,IX)+
     .         ZUD(5,I3)*ZKOD(5,IX))
     .                +ZF2*ZKO(I2,IY)*
     .        (+ZUD(I1,I2)*ZKOD(I1,IX)+ZUD(I3,I2)*ZKOD(I3,IX)
     .         +ZUD(I4,I2)*ZKOD(I4,IX)+ZUD(5,I2)*ZKOD(5,IX))
   30   CONTINUE
      END IF
      IF (ITYPE.EQ.4) THEN
        ZF1=ZD(I1,I4)/(ZD(I1,5)*ZD(5,I2)*PRO34*PRO134)
        ZF2=ZD(I1,I4)**2/(ZD(I1,5)*ZD(I3,I4)*PRO134*PR134K)
        DO 40 IX=1,2
          DO 40 IY=1,2
            Z1(IX,IY)=+ZF1*
     .        (ZKOD(I1,IX)*ZUD(I1,I3)+ZKOD(I4,IX)*ZUD(I4,I3))*
     .        (ZKO(I2,IY)*ZD(I2,I1)+ZKO(5,IY)*ZD(5,I1))
     .                +ZF2*ZKO(I2,IY)*
     .        (+ZUD(I1,5)*ZKOD(I1,IX)+ZUD(I3,5)*ZKOD(I3,IX)
     .         +ZUD(I4,5)*ZKOD(I4,IX))
   40   CONTINUE
      END IF
      CALL CONGAT(Z1,Z2)
      END
C
C**********************************************************************
C SUBROUTINE CONGAT(Z1,Z2) TAKES THE COMPLEX CONJUGATE
C SPINOR-TENSOR OF Z1 INTO Z2. USE NEGAEN TO CORRECT FOR
C AN ODD NUMBER OF QUARKS WITH NEGATIVE ENERGY.
C**********************************************************************
      SUBROUTINE CONGAT(Z1,Z2)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
      DIMENSION Z1(2,2),Z2(2,2)
C
      Z2(1,1)=NEGAEN*DCONJG(Z1(1,1))
      Z2(2,1)=NEGAEN*DCONJG(Z1(1,2))
      Z2(1,2)=NEGAEN*DCONJG(Z1(2,1))
      Z2(2,2)=NEGAEN*DCONJG(Z1(2,2))
      END
C
C**********************************************************************
C SUBROUTINE SETSPN(N,NRQUAR) DETERMINES THE CO-VARIANT WEYL-VD WAERDEN
C SPINORS AND ALL THE POSSIBLE SPINOR-INPRODUCTS.
C SPECIAL TREATMENT FOR PARTICLES 6 AND 7 (LEPTONS)
C PARTICLES WITH NEGATIVE ENERGY HAVE AN ARTIFICIAL MINUS
C SIGN IN THEIR DOTTED SPINORS.
C**********************************************************************
C CALLING CONVENTIONS:
C   N = NUMBER OF OF VECTORS (N MUST BE <= 7 )
C   COMMON /KINEMA/ P = ARRAY OF COVARIANT VECTORS (+,-,-,-) METRIC
C       NOTE: NONE OF THE VECTORS SHOULD BE PARALLEL TO THE Y-AXIS.
C**********************************************************************
C OUTPUT CONVENTIONS:
C   COMMON /DOTPR/ZUD(7,7),ZD(7,7),
C                 ZKO(7,2),ZKOD(7,2),NEGAEN
C   INPUT PARAMETERS REMAIN UNCHANGED.
C WITH:
C   ZUD(I,J)  = UNDOTTED SPINOR INPRODUCT
C   ZD(I,J)   = DOTTED SPINOR INPRODUCT
C   ZKO(I,1:2)  = UNDOTTED CO-VARIANT SPINORS
C   ZKOD(I,1:2) = DOTTED CO-VARIANT SPINORS
C   NEGAEN    = NUMBER OF QUARKS (PARTICLES 1-NRQUAR) WITH NEGATIVE
C               ENERGY. NEEDED WHEN COMPLEX-CONJUGATING.
C               NEGAEN=1,-1 WHEN THAT NUMBER IS EVEN,ODD
C**********************************************************************
      SUBROUTINE SETSPN(N,NRQUAR)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT INTEGER*4(I-N)
      IMPLICIT COMPLEX*16(Z)
      PARAMETER(NUP=7)
      COMMON /KINEMA/ P(0:3,1:NUP)
      DIMENSION ZUD(7,7),ZD(7,7),ZKO(7,2),ZKOD(7,2)
      COMMON /DOTPR/ ZUD,ZD,ZKO,ZKOD,NEGAEN
C
      NEGAEN=1
      DO 10 I=1,7
        IF((I.LE.N).OR.(I.GE.6))THEN
          ZKO(I,1)=(P(1,I)-(0D0,1D0)*P(3,I))
     .             /CDSQRT(DCMPLX(P(0,I)-P(2,I)))
          ZKO(I,2)=CDSQRT(DCMPLX(P(0,I)-P(2,I)))
          ZKOD(I,1)=DCONJG(ZKO(I,1))
          ZKOD(I,2)=DCONJG(ZKO(I,2))
          IF ((P(0,I).LT.0D0).AND.(I.LT.(NRQUAR+1))) THEN
            NEGAEN=-NEGAEN
          END IF
          IF (P(0,I).LT.0D0) THEN
            ZKOD(I,1)=-ZKOD(I,1)
            ZKOD(I,2)=-ZKOD(I,2)
          END IF
        END IF
   10 CONTINUE
C
      DO 20 I=1,N-1
        DO 20 J=I,N
          ZUD(I,J)=-ZKO(I,1)*ZKO(J,2)+ZKO(I,2)*ZKO(J,1)
          ZUD(J,I)=-ZUD(I,J)
          ZD(I,J)=-ZKOD(I,1)*ZKOD(J,2)+ZKOD(I,2)*ZKOD(J,1)
          ZD(J,I)=-ZD(I,J)
   20 CONTINUE
C
      END
C
C**********************************************************************
C FUNCTION RINPRO(I,J) DETERMINES THE MINKOWSKI INPRODUCT.
C THE PARAMETERS I AND J REFER TO THE MOMENTUM ARRAY /KINEMA/ P.
C BOTH VECTORS ARE CONTRA-VARIANT.
C**********************************************************************
      FUNCTION RINPRO(I,J)
      PARAMETER(NUP=7)
      IMPLICIT DOUBLE PRECISION(A-H,O-Y)
      IMPLICIT INTEGER (I-N)
      IMPLICIT COMPLEX*16(Z)
      COMMON /KINEMA/ P(0:3,1:NUP)
C
      RINPRO=P(0,I)*P(0,J)-P(1,I)*P(1,J)-P(2,I)*P(2,J)-P(3,I)*P(3,J)
      END
C
C**********************************************************************
C FUNCTION ZSPV(SMU,SNU) CALCULATES A SMU*SNU INPRODUCT IN
C SPINOR-TENSOR LANGUAGE. BOTH SMU AND SNU ARE CO-VARIANT TENSORS.
C THE FACTOR 2 IS DUE TO SPINOR TRANSFORMATION ALGEBRA.
C**********************************************************************
      FUNCTION ZSPV(SMU,SNU)
      COMPLEX*16 ZSPV,SMU(1:2,1:2),SNU(1:2,1:2)
      ZSPV=2D0*(+SMU(1,1)*SNU(2,2)
     .          +SMU(2,2)*SNU(1,1)
     .          -SMU(1,2)*SNU(2,1)
     .          -SMU(2,1)*SNU(1,2) )
      END
C
      SUBROUTINE RAMBO(N,ET,XM,P,WT)
C------------------------------------------------------
C
C                       RAMBO
C
C    RA(NDOM)  M(OMENTA)  B(EAUTIFULLY)  O(RGANIZED)
C
C    A DEMOCRATIC MULTI-PARTICLE PHASE SPACE GENERATOR
C    AUTHORS:  S.D. ELLIS,  R. KLEISS,  W.J. STIRLING
C    THIS IS VERSION 1.0 -  WRITTEN BY R. KLEISS
C
C    N  = NUMBER OF PARTICLES (>1, IN THIS VERSION <101)
C    ET = TOTAL CENTRE-OF-MASS ENERGY
C    XM = PARTICLE MASSES ( DIM=100 )
C    P  = PARTICLE MOMENTA ( DIM=(4,100) )
C    WT = WEIGHT OF THE EVENT
C
C------------------------------------------------------
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION XM(100),P(4,100),Q(4,100),Z(100),R(4),
     .   B(3),P2(100),XM2(100),E(100),V(100),IWARN(5)
      DATA ACC/1.D-14/,ITMAX/6/,IBEGIN/0/,IWARN/5*0/
C
C INITIALIZATION STEP: FACTORIALS FOR THE PHASE SPACE WEIGHT
      IF(IBEGIN.NE.0) GOTO 103
      IBEGIN=1
      TWOPI=8.*DATAN(1.D0)
      PO2LOG=DLOG(TWOPI/4.)
      Z(2)=PO2LOG
      DO 101 K=3,100
  101 Z(K)=Z(K-1)+PO2LOG-2.*DLOG(DFLOAT(K-2))
      DO 102 K=3,100
  102 Z(K)=(Z(K)-DLOG(DFLOAT(K-1)))
C
C CHECK ON THE NUMBER OF PARTICLES
  103 IF(N.GT.1.AND.N.LT.101) GOTO 104
      PRINT 1001,N
      STOP
C
C CHECK WHETHER TOTAL ENERGY IS SUFFICIENT; COUNT NONZERO MASSES
  104 XMT=0.
      NM=0
      DO 105 I=1,N
      IF(XM(I).NE.0.D0) NM=NM+1
  105 XMT=XMT+DABS(XM(I))
      IF(XMT.LE.ET) GOTO 201
      PRINT 1002,XMT,ET
      STOP
C
C THE PARAMETER VALUES ARE NOW ACCEPTED
C
C GENERATE N MASSLESS MOMENTA IN INFINITE PHASE SPACE
  201 DO 202 I=1,N
      C=2.*RN(1)-1.
      S=DSQRT(1.-C*C)
      F=TWOPI*RN(2)
      Q(4,I)=-DLOG(RN(3)*RN(4))
      Q(3,I)=Q(4,I)*C
      Q(2,I)=Q(4,I)*S*DCOS(F)
  202 Q(1,I)=Q(4,I)*S*DSIN(F)
C
C CALCULATE THE PARAMETERS OF THE CONFORMAL TRANSFORMATION
      DO 203 I=1,4
  203 R(I)=0.
      DO 204 I=1,N
      DO 204 K=1,4
  204 R(K)=R(K)+Q(K,I)
      RMAS=DSQRT(R(4)**2-R(3)**2-R(2)**2-R(1)**2)
      DO 205 K=1,3
  205 B(K)=-R(K)/RMAS
      G=R(4)/RMAS
      A=1./(1.+G)
      X=ET/RMAS
C
C TRANSFORM THE Q'S CONFORMALLY INTO THE P'S
      DO 207 I=1,N
      BQ=B(1)*Q(1,I)+B(2)*Q(2,I)+B(3)*Q(3,I)
      DO 206 K=1,3
  206 P(K,I)=X*(Q(K,I)+B(K)*(Q(4,I)+A*BQ))
  207 P(4,I)=X*(G*Q(4,I)+BQ)
C
C CALCULATE WEIGHT AND POSSIBLE WARNINGS
      WT=PO2LOG
      IF(N.NE.2) WT=(2.*N-4.)*DLOG(ET)+Z(N)
      IF(WT.GE.-180.D0) GOTO 208
      IF(IWARN(1).LE.5) PRINT 1004,WT
      IWARN(1)=IWARN(1)+1
  208 IF(WT.LE. 174.D0) GOTO 209
      IF(IWARN(2).LE.5) PRINT 1005,WT
      IWARN(2)=IWARN(2)+1
C
C RETURN FOR WEIGHTED MASSLESS MOMENTA
  209 IF(NM.NE.0) GOTO 210
      WT=DEXP(WT)
      RETURN
C
C MASSIVE PARTICLES: RESCALE THE MOMENTA BY A FACTOR X
  210 XMAX=DSQRT(1.-(XMT/ET)**2)
      DO 301 I=1,N
      XM2(I)=XM(I)**2
  301 P2(I)=P(4,I)**2
      ITER=0
      X=XMAX
      ACCU=ET*ACC
  302 F0=-ET
      G0=0.
      X2=X*X
      DO 303 I=1,N
      E(I)=DSQRT(XM2(I)+X2*P2(I))
      F0=F0+E(I)
  303 G0=G0+P2(I)/E(I)
      IF(DABS(F0).LE.ACCU) GOTO 305
      ITER=ITER+1
      IF(ITER.LE.ITMAX) GOTO 304
      PRINT 1006,ITMAX
      GOTO 305
  304 X=X-F0/(X*G0)
      GOTO 302
  305 DO 307 I=1,N
      V(I)=X*P(4,I)
      DO 306 K=1,3
  306 P(K,I)=X*P(K,I)
  307 P(4,I)=E(I)
C
C CALCULATE THE MASS-EFFECT WEIGHT FACTOR
      WT2=1.
      WT3=0.
      DO 308 I=1,N
      WT2=WT2*V(I)/E(I)
  308 WT3=WT3+V(I)**2/E(I)
      WTM=(2.*N-3.)*DLOG(X)+DLOG(WT2/WT3*ET)
C
C RETURN FOR  WEIGHTED MASSIVE MOMENTA
      WT=WT+WTM
      IF(WT.GE.-180.D0) GOTO 309
      IF(IWARN(3).LE.5) PRINT 1004,WT
      IWARN(3)=IWARN(3)+1
  309 IF(WT.LE. 174.D0) GOTO 310
      IF(IWARN(4).LE.5) PRINT 1005,WT
      IWARN(4)=IWARN(4)+1
  310 WT=DEXP(WT)
      RETURN
C
 1001 FORMAT(' RAMBO FAILS: # OF PARTICLES =',I5,' IS NOT ALLOWED')
 1002 FORMAT(' RAMBO FAILS: TOTAL MASS =',D15.6,' IS NOT',
     . ' SMALLER THAN TOTAL ENERGY =',D15.6)
 1004 FORMAT(' RAMBO WARNS: WEIGHT = EXP(',F20.9,') MAY UNDERFLOW')
 1005 FORMAT(' RAMBO WARNS: WEIGHT = EXP(',F20.9,') MAY  OVERFLOW')
 1006 FORMAT(' RAMBO WARNS:',I3,' ITERATIONS DID NOT GIVE THE',
     . ' DESIRED ACCURACY =',D15.6)
      END
C
      SUBROUTINE VEGAS(FXN,AVGI,SD,CHI2A)
C
C   SUBROUTINE PERFORMS N-DIMENSIONAL MONTE CARLO INTEG'N
C      - BY G.P. LEPAGE   SEPT 1976/(REV)APR 1978
C
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/BVEG1/XL(10),XU(10),ACC,NDIM,NCALL,ITMX,NPRN
      COMMON/BVEG2/XI(50,10),SI,SI2,SWGT,SCHI,NDO,IT
      DIMENSION D(50,10),DI(50,10),XIN(50),R(50),DX(10),DT(10),X(10)
     1   ,KG(10),IA(10)
      REAL*8 QRAN(10)
      EXTERNAL FXN
      DATA NDMX/50/,ALPH/1.5D0/,ONE/1D0/,MDS/1/
C
      NDO=1
      DO 1 J=1,NDIM
1     XI(1,J)=ONE
C
      ENTRY VEGAS1(FXN,AVGI,SD,CHI2A)
C         - INITIALIZES CUMMULATIVE VARIABLES, BUT NOT GRID
      IT=0
      SI=0.
      SI2=SI
      SWGT=SI
      SCHI=SI
C
      ENTRY VEGAS2(FXN,AVGI,SD,CHI2A)
C         - NO INITIALIZATION
      ND=NDMX
      NG=1
      IF(MDS.EQ.0) GO TO 2
      NG=(NCALL/2.)**(1./NDIM)
      MDS=1
      IF((2*NG-NDMX).LT.0) GO TO 2
      MDS=-1
      NPG=NG/NDMX+1
      ND=NG/NPG
      NG=NPG*ND
2     K=NG**NDIM
      NPG=NCALL/K
      IF(NPG.LT.2) NPG=2
      CALLS=NPG*K
      DXG=ONE/NG
      DV2G=(CALLS*DXG**NDIM)**2/NPG/NPG/(NPG-ONE)
      XND=ND
      NDM=ND-1
      DXG=DXG*XND
      XJAC=ONE/CALLS
      DO 3 J=1,NDIM
      DX(J)=XU(J)-XL(J)
3     XJAC=XJAC*DX(J)
C
C   REBIN PRESERVING BIN DENSITY
C
      IF(ND.EQ.NDO) GO TO 8
      RC=NDO/XND
      DO 7 J=1,NDIM
      K=0
      XN=0.
      DR=XN
      I=K
4     K=K+1
      DR=DR+ONE
      XO=XN
      XN=XI(K,J)
5     IF(RC.GT.DR) GO TO 4
      I=I+1
      DR=DR-RC
      XIN(I)=XN-(XN-XO)*DR
      IF(I.LT.NDM) GO TO 5
      DO 6 I=1,NDM
6     XI(I,J)=XIN(I)
7     XI(ND,J)=ONE
      NDO=ND
C
8     IF(NPRN.NE.0) WRITE(6,200) NDIM,CALLS,IT,ITMX,ACC,MDS,ND
     1                           ,(XL(J),XU(J),J=1,NDIM)
C
      ENTRY VEGAS3(FXN,AVGI,SD,CHI2A)
C         - MAIN INTEGRATION LOOP
9     IT=IT+1
      TI=0.
      TSI=TI
      DO 10 J=1,NDIM
      KG(J)=1
      DO 10 I=1,ND
      D(I,J)=TI
10    DI(I,J)=TI
C
11    FB=0.
      F2B=FB
      K=0
12    K=K+1
      CALL ARAN9(QRAN,NDIM)
      WGT=XJAC
      DO 15 J=1,NDIM
      XN=(KG(J)-QRAN(J))*DXG+ONE
      IA(J)=XN
      IF(IA(J).GT.1) GO TO 13
      XO=XI(IA(J),J)
      RC=(XN-IA(J))*XO
      GO TO 14
13    XO=XI(IA(J),J)-XI(IA(J)-1,J)
      RC=XI(IA(J)-1,J)+(XN-IA(J))*XO
14    X(J)=XL(J)+RC*DX(J)
15    WGT=WGT*XO*XND
C
      F=WGT
      F=F*FXN(X,WGT)
      F2=F*F
      FB=FB+F
      F2B=F2B+F2
      DO 16 J=1,NDIM
      DI(IA(J),J)=DI(IA(J),J)+F
16    IF(MDS.GE.0) D(IA(J),J)=D(IA(J),J)+F2
      IF(K.LT.NPG) GO TO 12
C
      F2B=DSQRT(F2B*NPG)
      F2B=(F2B-FB)*(F2B+FB)
      TI=TI+FB
      TSI=TSI+F2B
      IF(MDS.GE.0) GO TO 18
      DO 17 J=1,NDIM
17    D(IA(J),J)=D(IA(J),J)+F2B
18    K=NDIM
19    KG(K)=MOD(KG(K),NG)+1
      IF(KG(K).NE.1) GO TO 11
      K=K-1
      IF(K.GT.0) GO TO 19
C
C   FINAL RESULTS FOR THIS ITERATION
C
      TSI=TSI*DV2G
      TI2=TI*TI
      WGT=TI2/TSI
      SI=SI+TI*WGT
      SI2=SI2+TI2
      SWGT=SWGT+WGT
      SCHI=SCHI+TI2*WGT
      AVGI=SI/SWGT
      SD=SWGT*IT/SI2
      CHI2A=SD*(SCHI/SWGT-AVGI*AVGI)/(IT-.999)
      SD=DSQRT(ONE/SD)
C
      IF(NPRN.EQ.0) GO TO 21
      TSI=DSQRT(TSI)
      WRITE(6,201) IT,TI,TSI,AVGI,SD,CHI2A
      IF(NPRN.GE.0) GO TO 21
      DO 20 J=1,NDIM
20    WRITE(6,202) J,(XI(I,J),DI(I,J),D(I,J),I=1,ND)
C
C   REFINE GRID
C
21    DO 23 J=1,NDIM
      XO=D(1,J)
      XN=D(2,J)
      D(1,J)=(XO+XN)/2.
      DT(J)=D(1,J)
      DO 22 I=2,NDM
      D(I,J)=XO+XN
      XO=XN
      XN=D(I+1,J)
      D(I,J)=(D(I,J)+XN)/3.
22    DT(J)=DT(J)+D(I,J)
      D(ND,J)=(XN+XO)/2.
23    DT(J)=DT(J)+D(ND,J)
C
      DO 28 J=1,NDIM
      RC=0.
      DO 24 I=1,ND
      R(I)=0.
      IF(D(I,J).LE.0.) GO TO 24
      XO=DT(J)/D(I,J)
      R(I)=((XO-ONE)/XO/DLOG(XO))**ALPH
24    RC=RC+R(I)
      RC=RC/XND
      K=0
      XN=0.
      DR=XN
      I=K
25    K=K+1
      DR=DR+R(K)
      XO=XN
      XN=XI(K,J)
26    IF(RC.GT.DR) GO TO 25
      I=I+1
      DR=DR-RC
      XIN(I)=XN-(XN-XO)*DR/R(K)
      IF(I.LT.NDM) GO TO 26
      DO 27 I=1,NDM
27    XI(I,J)=XIN(I)
28    XI(ND,J)=ONE
C
      IF(IT.LT.ITMX.AND.ACC*DABS(AVGI).LT.SD) GO TO 9
200   FORMAT('0INPUT PARAMETERS FOR VEGAS:  NDIM=',I3,'  NCALL=',F8.0
     1    /28X,'  IT=',I5,'  ITMX=',I5/28X,'  ACC=',G9.3
     2    /28X,'  MDS=',I3,'   ND=',I4/28X,'  (XL,XU)=',
     3    (T40,'( ',G12.6,' , ',G12.6,' )'))
201   FORMAT(///' INTEGRATION BY VEGAS' / '0ITERATION NO.',I3,
     1    ':   INTEGRAL =',G14.8/21X,'STD DEV  =',G10.4 /
     2    ' ACCUMULATED RESULTS:   INTEGRAL =',G14.8 /
     3    24X,'STD DEV  =',G10.4 / 24X,'CHI**2 PER IT''N =',G10.4)
202   FORMAT('0DATA FOR AXIS',I2 / ' ',6X,'X',7X,'  DELT I  ',
     1    2X,' CONV''CE  ',11X,'X',7X,'  DELT I  ',2X,' CONV''CE  '
     2   ,11X,'X',7X,'  DELT I  ',2X,' CONV''CE  ' /
     2    (' ',3G12.4,5X,3G12.4,5X,3G12.4))
      RETURN
      END
      SUBROUTINE SAVE(NDIM)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/BVEG2/XI(50,10),SI,SI2,SWGT,SCHI,NDO,IT
C
C   STORES VEGAS DATA (UNIT 7) FOR LATER RE-INITIALIZATION
C
      WRITE(7,200) NDO,IT,SI,SI2,SWGT,SCHI,
     1             ((XI(I,J),I=1,NDO),J=1,NDIM)
      RETURN
      ENTRY RESTR(NDIM)
C
C   ENTERS INITIALIZATION DATA FOR VEGAS
C
      READ(7,200) NDO,IT,SI,SI2,SWGT,SCHI,
     1            ((XI(I,J),I=1,NDO),J=1,NDIM)
200   FORMAT(2I8,4Z16/(5Z16))
      RETURN
      END
C
      SUBROUTINE ARAN9(QRAN,NDIM)
      REAL*8 QRAN(10)
      DO 1 I=1,NDIM
    1 CALL R2455(QRAN(I))
      RETURN
      END
C
      SUBROUTINE R2455(RAN)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION N(55)
      DATA N/
     . 980629335, 889272121, 422278310,1042669295, 531256381,
     . 335028099,  47160432, 788808135, 660624592, 793263632,
     . 998900570, 470796980, 327436767, 287473989, 119515078,
     . 575143087, 922274831,  21914605, 923291707, 753782759,
     . 254480986, 816423843, 931542684, 993691006, 343157264,
     . 272972469, 733687879, 468941742, 444207473, 896089285,
     . 629371118, 892845902, 163581912, 861580190,  85601059,
     . 899226806, 438711780, 921057966, 794646776, 417139730,
     . 343610085, 737162282,1024718389,  65196680, 954338580,
     . 642649958, 240238978, 722544540, 281483031,1024570269,
     . 602730138, 915220349, 651571385, 405259519, 145115737/
      DATA M/1073741824/
      DATA RM/0.9313225746154785D-09/
      DATA K/55/,L/31/
      IF(K.EQ.55) THEN
         K=1
      ELSE
         K=K+1
      ENDIF
      IF(L.EQ.55) THEN
         L=1
      ELSE
         L=L+1
      ENDIF
      J=N(L)-N(K)
      IF(J.LT.0) J=J+M
      N(K)=J
      RAN=J*RM
      END
C
      FUNCTION RN(IDMY)
      REAL*8 RN,RAN
      CALL R2455(RAN)
      RN=RAN
      END
 
 
      SUBROUTINE HISTO(I,ISTAT,X,X0,X1,WEIGHT,LINLOG,TITLE,IUNIT,NX)
*     ----------------
* STEVEN VAN DER MARCK, MARCH, 29TH, 1989
* I      = NUMBER OF THIS PARTICULAR HISTOGRAM
* ISTAT  = 0    : CLEAR THE ARRAYS
*          1    : FILL  THE ARRAYS
*          ELSE : PRINT A HISTOGRAM
* X      = X-VALUE TO BE PLACED IN A BIN OF THE HISTOGRAM
* X0     = THE MINIMUM FOR X IN THE HISTOGRAM
* X1     = THE MAXIMUM FOR X IN THE HISTOGRAM
* WEIGHT = THE WEIGHT ASSIGNED TO THIS VALUE OF X
* LINLOG = 1    : LINEAR      HISTOGRAM
*          ELSE : LOGARITHMIC HISTOGRAM
*          IF A LINEAR HISTO HAS ONLY ONE PEAK THAT IS TOO SHARP,
*          THIS ROUTINE WILL AUTOMATICALLY SWITCH TO A LOG. HISTO.
* TITLE  = TITLE OF THIS PARTICULAR HISTOGRAM ( CHARACTER*(*) )
* IUNIT  = UNIT NUMBER TO WRITE THIS HISTOGRAM TO
* NX     = THE NUMBER OF DIVISIONS ON THE X-AXIS FOR THIS HISTOGRAM
*          (NX SHOULD NOT BE GREATER THAN 50 IN THIS VERSION)
*
* WHEN ISTAT = 0    : NO OTHER VARIABLES ARE USED.
*            = 1    : I, X, X0, X1, WEIGHT, AND NX ARE USED.
*            = ELSE : I, LINLOG, TITLE AND IUNIT ARE USED.
*
      IMPLICIT LOGICAL(A-Z)
      INTEGER N,NX,NY, I,ISTAT,LINLOG,IUNIT, J1,J2, IX,IY
      DOUBLE PRECISION X,X0,X1,WEIGHT, DFLOAT,Z
      CHARACTER*(*) TITLE
      PARAMETER( N = 20 , NY = 20 )
      CHARACTER LINE(NY),BLANK,STAR
      DOUBLE PRECISION Y(N,50), YMAX(N), BOUND0(N), BOUND1(N),
     +                 XMIN(N), XMAX(N)
      INTEGER IUNDER(N), IIN(N), IOVER(N), NRBINS(N)
      SAVE
      DATA BLANK/' '/STAR/'*'/
      IF(ISTAT .EQ. 0) THEN
        DO 10 J1 = 1 , N
          BOUND0(J1) = 0D0
          BOUND1(J1) = 0D0
          XMIN  (J1) = 0D0
          XMAX  (J1) = 0D0
          YMAX  (J1) = 0D0
          IUNDER(J1) = 0
          IIN   (J1) = 0
          IOVER (J1) = 0
          NRBINS(J1) = 0
          DO 20 J2 = 1 , 50
             Y(J1,J2) = 0D0
   20     CONTINUE
   10   CONTINUE
      ELSEIF(ISTAT.EQ.1) THEN
        BOUND0(I) = X0
        BOUND1(I) = X1
        IF(NRBINS(I) .EQ. 0) NRBINS(I) = NX
        IF(X.LT.X0)THEN
          IUNDER(I) = IUNDER(I) + 1
          IF(X .LT. XMIN(I))           XMIN(I) = X
          IF(DABS(XMIN(I)) .LT. 1D-16) XMIN(I) = X
        ELSEIF(X.GT.X1)THEN
          IOVER(I) = IOVER(I) + 1
          IF(X .GT. XMAX(I))           XMAX(I) = X
          IF(DABS(XMAX(I)) .LT. 1D-16) XMAX(I) = X
        ELSE
          IIN(I) = IIN(I) + 1
          IX     = IDINT((X-X0)/(X1-X0)*NRBINS(I)) + 1
          IF(IX.EQ.NRBINS(I)+1) IX = NRBINS(I)
          Y(I,IX) = Y(I,IX) + WEIGHT
          IF(Y(I,IX).GT.YMAX(I)) YMAX(I) = Y(I,IX)
        ENDIF
      ELSE
        WRITE(IUNIT,30)I,IIN(I),IUNDER(I),IOVER(I)
   30   FORMAT(//,' HISTOGRAM NR.',I2,'  POINTS IN:',I8,
     +         '  UNDER:',I6,'  OVER:',I6)
        WRITE(IUNIT,*)' ',TITLE
        IF(YMAX(I).LE.0.D0) RETURN
        IF(LINLOG .EQ. 1) THEN
          IX = 0
          DO 35 J1 = 1 , NRBINS(I)
            IF(Y(I,J1)/YMAX(I)*NY .GT. 1D0) IX = IX + 1
   35     CONTINUE
        ENDIF
        IF(IX .LE. 2 .OR. LINLOG .NE. 1) THEN
          IX = 2
        ELSE
          IX = 1
        ENDIF
        WRITE(IUNIT,40) BOUND0(I)
   40   FORMAT('  ',G11.4,' ',20('-'))
        DO 90 J1 = 1 , NRBINS(I)
          IF(IX.EQ.1) THEN
            IY=IDINT(Y(I,J1)/YMAX(I)*DFLOAT(NY))+1
          ELSE
            IF(Y(I,J1).LE.1.D0)THEN
              IY=1
            ELSE
              IY=IDINT(DLOG(Y(I,J1))/DLOG(YMAX(I))*DFLOAT(NY))+1
            ENDIF
          ENDIF
          IF(IY .EQ. NY+1) IY = NY
          DO 50 J2 = 1 , NY
            IF(J2.EQ.IY)THEN
              LINE(J2)=STAR
            ELSE
              LINE(J2)=BLANK
            ENDIF
   50     CONTINUE
          Z = BOUND0(I) + J1/DFLOAT(NRBINS(I))*(BOUND1(I)-BOUND0(I))
          IF(J1.EQ.IDINT(DFLOAT(NRBINS(I))/2.D0))THEN
            IF(IX.EQ.1) THEN
              WRITE(IUNIT,60)(LINE(J2),J2=1,NY),Z,Y(I,J1)
   60         FORMAT('   LINEAR    I',20(A),'I',5X,G15.4,3X,G15.4)
            ELSE
              WRITE(IUNIT,70)(LINE(J2),J2=1,NY),Z,Y(I,J1)
   70         FORMAT(' LOGARITHMIC I',20(A),'I',5X,G15.4,3X,G15.4)
            ENDIF
          ELSE
            WRITE(IUNIT,80)(LINE(J2),J2=1,NY),Z,Y(I,J1)
   80       FORMAT(' ',12X,'I',20(A),'I',5X,G15.4,3X,G15.4)
          ENDIF
   90   CONTINUE
        WRITE(IUNIT,40)BOUND1(I)
        IF(DABS(XMIN(I)) .GE. 1D-16)
     +    WRITE(IUNIT,*)' THE MINIMUM VALUE THAT OCCURRED WAS ',XMIN(I)
        IF(DABS(XMAX(I)) .GE. 1D-16)
     +    WRITE(IUNIT,*)' THE MAXIMUM VALUE THAT OCCURRED WAS ',XMAX(I)
      ENDIF
      END
 

