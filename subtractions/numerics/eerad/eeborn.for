C BORN AMPLITUDE VOOR E+ E- NAAR Q QB (1=UP, 2=DOWN)
      FUNCTION ANAL(S,SW2,RMZ,RGZ,ITYPE)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION Q(2),V(2),A(2)
      Q(1)=+2D0/3D0
      Q(2)=-1D0/3D0
      V(1)=1D0-8D0/3D0*SW2
      V(2)=-1D0+4D0/3D0*SW2
      A(1)=1D0
      A(2)=-1D0
      AM=1D0/137D0
      PI=4D0*DATAN(1D0)
      AE=-1D0
      VE=-1D0+4D0*SW2
 
      GF=DSQRT(2D0)*AM/(4D0*PI)/(8D0*SW2*RMZ*RMZ*(1D0-SW2))
      G=DSQRT(2D0)*GF/(16D0*PI*AM)
 
      P1=Q(ITYPE)**2*8D0/3D0
      P2=(-2D0*Q(ITYPE)*G*S*(S/RMZ/RMZ-1D0)*(VE*V(ITYPE)*8D0/3D0+
     .   2D0*AE*A(ITYPE)*2D0) )/( (S/RMZ/RMZ-1D0)**2+(RGZ/RMZ)**2 )
      P3=( S**2*G**2* ((VE**2+AE**2)*(V(ITYPE)**2+A(ITYPE)**2)*8D0/3D0
     . + 8D0*VE*AE*A(ITYPE)*V(ITYPE)*2D0 ) )/
     .  ( (S/RMZ/RMZ-1D0)**2 + (RGZ/RMZ)**2 )
 
      FACTOR=3D0*PI*AM**2/2D0/S*3.89385D8
      P1=P1*FACTOR
      P2=P2*FACTOR
      P3=P3*FACTOR
      ANAL=P1+P2+P3
      END

